////
//// Created by Priya Ravichander on 6/10/21.
////
//#include <regex>
//#include <gtest/gtest.h>
//#include "../include/MysqlClient.h"
//#include "../include/ripeatlashandler.h"
//#include "../include/Validator.h"
//
//#include "../include/dns/dns_rrset.h"
//
//#include "../include/dns/dns_dnskey.h"
//#include "../include/dns/dns_rrsig.h"
//#include "../include/evaluation.h"
//#include "../include/DnssecRRset.h"
//#include<unistd.h>
//#include <map>
//
//
//TEST(example, testMysqlConnection) {
//
//
//   MysqlClient mysqlClient;
//    MYSQL* connection = mysqlClient.getConnection("dnssec", "127.0.0.1", 3306);
//    cout << connection;
//    mysql_close(connection);
//
////   ASSERT_NE(NULL, connection);
//}
//
//TEST(example, testSOATableInsertion) {
//
//    RipeAtlasHandler ripeAtlasHandler;
//
//    const string &result = ripeAtlasHandler.getRipeAtlasMeasurement(30800676);
//
//        Validator validator;
//
//        ProbeResult probeResult = validator.extractSOAData(result);
//
//    MysqlClient mysqlClient;
//
//        for (ProbeInfo *probeInfo: probeResult.probeInfo) {
//            for (Measurement *measurement: probeInfo->measurement) {
//                DnsRRset rrset;
//
//
//                mysqlClient.insertDomainData("example.com", measurement->soaName, probeInfo->probeid, probeInfo->timestamp);
//            }
//        }
//}
//
//
//TEST(example, testDNSKEYTableInsertion) {
//
//    RipeAtlasHandler ripeAtlasHandler;
//
//    MysqlClient mysqlClient;
//
//
//    const string &result = ripeAtlasHandler.getRipeAtlasMeasurement(30806274);
//
//    string rrsigProbeResult = ripeAtlasHandler.getRipeAtlasMeasurement(30806276);
//
////    cout << "\n"<<result;
//
//    Validator validator;
//
//    ProbeResult probeResult = validator.extractRipeAtlasData(result);
//
//    Evaluation eval;
//    map<string, list<DnsDnskey *>> dnskeyMap = eval.groupDnskeyRecords(probeResult);
//
//
//
//
//    cout <<"\n\n\n";
//
//    ProbeResult rrsigResult = validator.extractRipeAtlasData(rrsigProbeResult);
//
//    map<string, list<DnsRR *>> rrsigMap = eval.groupRRsigRecords(rrsigResult);
//
//
//
//    map<string, list<DnsDnskey *>>::iterator dnskeyMapIter;
//    map<uint16_t, list<DnssecRRset *> > dnssecRRsetMap;
//    for (dnskeyMapIter = dnskeyMap.begin();
//         dnskeyMapIter != dnskeyMap.end(); dnskeyMapIter++) {
//        list<DnsDnskey *>::iterator dnskeyIter;
//cout <<"\n1>>>>>>>";
//        for(dnskeyIter = dnskeyMapIter->second.begin(); dnskeyIter!=dnskeyMapIter->second.end(); dnskeyIter++){
//            string probeid_subid = dnskeyMapIter->first;
//            if(rrsigMap.find(probeid_subid) != rrsigMap.end()){
//                list<DnsRR *>::iterator rrsigIter;
//                cout <<"\n2>>>>>>>";
//                for(rrsigIter = rrsigMap.find(probeid_subid)->second.begin(); rrsigIter != rrsigMap.find(
//                        probeid_subid)->second.end(); rrsigIter++){
//                    if((dynamic_cast<DnsDnskey *>(*dnskeyIter)->calcKeyTag()) == (dynamic_cast<DnsRrsig *>(*rrsigIter))->getKeyTag()) {
//                        DnssecRRset* dnssecRRset = new DnssecRRset();
//                        dnssecRRset->dnskey = (dynamic_cast<DnsDnskey *>(*dnskeyIter));
//                        dnssecRRset->rrsig = (dynamic_cast<DnsRrsig *>(*rrsigIter));
//                        cout <<"\n3>>>>>>>";
//                        int end;
//                        unsigned long abc = probeid_subid.find('_', end);
//
//                        dnssecRRset->probeId = stol(probeid_subid.substr(0, abc - 0));
//                        dnssecRRset->subId = stol(probeid_subid.substr(abc + 1, probeid_subid.length() - 1));
//                        dnssecRRset->keytag = dnssecRRset->rrsig->getKeyTag();
//                        dnssecRRset->domainId = "1";
//                        map<uint16_t, list<DnssecRRset*> >::iterator dnssecRRsetMapIter =
//                                dnssecRRsetMap.find(dnssecRRset->keytag);
//
//                        if (dnssecRRsetMapIter != dnssecRRsetMap.end()) {
//                            cout <<"\n4>>>>>>>";
//                            dnssecRRsetMapIter->second.push_back(dnssecRRset);
//
//                        } else {
//                            cout <<"\n5>>>>>>>";
//                            list<DnssecRRset*> dnsseclist;
//                            dnsseclist.push_back(dnssecRRset);
//                            dnssecRRsetMap.insert(
//                                    pair<uint16_t, list<DnssecRRset*> >(
//                                            dnssecRRset->rrsig->getKeyTag(), dnsseclist));
//                        }
//
//                    }
//                }
//            }
//        }
//    }
//    cout <<"\n6>>>>>>>";
//    map<uint16_t, list<DnssecRRset*> >::iterator dnssecRRsetMapIter2;
//    long matchId = 0;
//    for (dnssecRRsetMapIter2 = dnssecRRsetMap.begin();
//         dnssecRRsetMapIter2 != dnssecRRsetMap.end(); dnssecRRsetMapIter2++) {
//        cout <<"We got this!!!!!!"<< dnssecRRsetMapIter2->first;
//        list<DnssecRRset*>::iterator valueIter;
//        int timestamp = std::time(0);
//        cout <<"\n"<< timestamp;
//        matchId++;
//        for(valueIter = dnssecRRsetMapIter2->second.begin();valueIter != dnssecRRsetMapIter2->second.end();valueIter++) {
//            mysqlClient.insertDNSSECMeasurementRecords((*valueIter)->probeId,timestamp, (*valueIter)->domainId,(*valueIter)->dnskey->getDnskeyId(), (*valueIter)->rrsig->getRRSigId(),(*valueIter)->subId, matchId);
//        }
//
//    }
//
//}
