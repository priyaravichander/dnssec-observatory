//
// Created by Priya Ravichander on 6/13/21.
//

#include "../../include/global_variables.h"
#include "string"
using namespace std;
long nextDnskeyId = 0;
long nextRRsigId = 0;
long nextDomainId = 0;
std::map<long, string> probeMap =    {{1001552, "Arizona"},
                                           {51310,   "Georgia"},
                                           {27214,   "Texas"},
                                           {25953,   "Minnesota"},
                                           {54560,   "Washington"},
                                           {6292,    "Colorado"},
                                           {53807,   "Illinois"},
                                           {50253,   "Massachusetts"},
                                           {10415,   "California"},
                                           {10506,   "District of Columbia"}};