//
// Created by Priya Ravichander on 6/8/21.
//

#include <iostream>
#include "../../include/evaluation.h"
#include "../../include/dns/dns_dnskey.h"
#include "../../include/dns/dns_rrsig.h"
#include "../../include/MysqlClient.h"
#include "../../include/global_variables.h"
#include "../../include/DnssecMeasurement.h"

using namespace std;

map<uint16_t, list<DnssecRRset *> >
Evaluation::groupMatchingKeys(map<string, list<DnsDnskey *>> dnskeyMap, map<string, list<DnsRR *>> rrsigMap,
                              map<uint16_t, list<string>> *keyAndProbeIdListMap,
                              map<string, list<uint16_t>> *probeIdKeytagListMap) {
    map<string, list<DnsDnskey *>>::iterator dnskeyMapIter;
    map<uint16_t, list<DnssecRRset *> > dnssecRRsetMap;

    for (dnskeyMapIter = dnskeyMap.begin();
         dnskeyMapIter != dnskeyMap.end(); dnskeyMapIter++) {
        list<DnsDnskey *>::iterator dnskeyIter;
        for (dnskeyIter = dnskeyMapIter->second.begin(); dnskeyIter != dnskeyMapIter->second.end(); dnskeyIter++) {
            string probeid_subid = dnskeyMapIter->first;
            if (rrsigMap.find(probeid_subid) != rrsigMap.end()) {
                list<DnsRR *>::iterator rrsigIter;

                for (rrsigIter = rrsigMap.find(probeid_subid)->second.begin(); rrsigIter != rrsigMap.find(
                        probeid_subid)->second.end(); rrsigIter++) {
                    if ((dynamic_cast<DnsDnskey *>(*dnskeyIter)->calcKeyTag()) ==
                        (dynamic_cast<DnsRrsig *>(*rrsigIter))->getKeyTag()) {
                        DnssecRRset *dnssecRRset = new DnssecRRset();
                        dnssecRRset->dnskey = (dynamic_cast<DnsDnskey *>(*dnskeyIter));
                        dnssecRRset->rrsig = (dynamic_cast<DnsRrsig *>(*rrsigIter));

                        int end;
                        unsigned long abc = probeid_subid.find('_', end);

                        dnssecRRset->probeId = stol(probeid_subid.substr(0, abc - 0));
                        dnssecRRset->subId = stol(probeid_subid.substr(abc + 1, probeid_subid.length() - 1));
                        dnssecRRset->keytag = dnssecRRset->rrsig->getKeyTag();
                        dnssecRRset->domainId = nextDomainId;
                        map<uint16_t, list<DnssecRRset *> >::iterator dnssecRRsetMapIter =
                                dnssecRRsetMap.find(dnssecRRset->keytag);
                        map<uint16_t, list<string> >::iterator keytagProbeIdMapIter = keyAndProbeIdListMap->find(
                                dnssecRRset->keytag);
                        map<string, list<uint16_t> >::iterator probeIdKeytagMapIter = probeIdKeytagListMap->find(
                                probeid_subid);
                        if (probeIdKeytagMapIter != probeIdKeytagListMap->end()) {
                            probeIdKeytagMapIter->second.push_back(dnssecRRset->keytag);
                        } else {
                            list<uint16_t> keytagList;
                            keytagList.push_back(dnssecRRset->keytag);
                            probeIdKeytagListMap->insert(pair<string, list<uint16_t>>(probeid_subid, keytagList));

                        }
                        if (dnssecRRsetMapIter != dnssecRRsetMap.end()) {
//                            cout <<"\n4>>>>>>>";
                            dnssecRRsetMapIter->second.push_back(dnssecRRset);
                            keytagProbeIdMapIter->second.push_back(probeid_subid);


                        } else {
//                            cout <<"\n5>>>>>>>";
                            list<DnssecRRset *> dnsseclist;
                            list<string> probeIdList;

                            dnsseclist.push_back(dnssecRRset);
                            probeIdList.push_back(probeid_subid);
                            keyAndProbeIdListMap->insert(
                                    pair<uint16_t, list<string>>(dnssecRRset->keytag, probeIdList));
                            dnssecRRsetMap.insert(
                                    pair<uint16_t, list<DnssecRRset *> >(
                                            dnssecRRset->rrsig->getKeyTag(), dnsseclist));

                        }


                    }
                }
            }
        }
    }
    return dnssecRRsetMap;
}

map<string, map<std::string, list<struct DnsDnskey *>>>
Evaluation::groupDnskeyRecords(map<string, ProbeResult> probeResult, long domainId) {
    MysqlClient mysqlClient;
    map<string, map<string, list<DnsDnskey *>>> dnskeyGroupMap;
    for (map<string, ProbeResult>::iterator probeResultIter = probeResult.begin();
         probeResultIter != probeResult.end(); probeResultIter++) {
        map<string, list<DnsDnskey *> > dnskeyMap;
        map<uint16_t, long> keymap;
        string dnskeyRecords = "";

        string dnskeyRelationships = "";

        for (ProbeInfo *info: probeResultIter->second.probeInfo) {
            for (Measurement *m: info->measurement) {

                RRList_t dnskeylist;
                list<DnsDnskey *> listOfDNSKeys;
                m->result->getAnswers(dnskeylist);
                for (RRIter_t iter = dnskeylist.begin();
                     dnskeylist.end() != iter;
                     iter++) {
                    DnsDnskey *dnskey = static_cast<DnsDnskey *>(*iter);

                    if (keymap.find(dnskey->calcKeyTag()) == keymap.end()) {
//                        dnskey->setDnskeyId(++nextDnskeyId);
                        dnskeyRecords +=
                                "('"  +to_string(std::time(nullptr))+ "','"+
                                to_string(dnskey->getFlags()) + "','" +
                                to_string((dnskey)->getAlgo()) + "','" + (dnskey)->getKey()
                                + "','" + to_string(dnskey->calcKeyTag()) +
                                "'),";
                        keymap.insert(pair<uint16_t, long>(dnskey->calcKeyTag(), dnskey->getDnskeyId()));
                    }
                    dnskeyRelationships +=
                            "('" + to_string(std::time(nullptr)) + "','" + to_string(std::time(nullptr)) + "','" +to_string(info->probeid) + "','" +
                            to_string(m->subid) + "','" +
                            to_string(dnskey->calcKeyTag()) + "','" + to_string(domainId)
                            + "'),";

                    listOfDNSKeys.push_back(dnskey);
                }
                string key = std::to_string(info->probeid) + "_" + to_string(m->subid);
//                dnskeyMap.insert(pair<string, list<DnsDnskey *> >(key, listOfDNSKeys));
                key = m->destinationAddress;
                if(dnskeyMap.find(key) == dnskeyMap.end())
                    dnskeyMap.insert(pair<string, list<DnsDnskey *> >(key, listOfDNSKeys));
                else{
                    dnskeyMap.find(key)->second.insert(dnskeyMap.find(key)->second.end(),listOfDNSKeys.begin(), listOfDNSKeys.end());
                }

            }
        }
        if (!dnskeyRelationships.empty() && !dnskeyRecords.empty()) {
//            mysqlClient.insertDnsKeyRecords(dnskeyRelationships, dnskeyRecords);
        }

        dnskeyGroupMap.insert(pair<string, map<string, list<DnsDnskey *>>>(probeResultIter->first, dnskeyMap));
    }
    return dnskeyGroupMap;
}

map<string, map<std::string, list<struct DnsRR *>>>
Evaluation::groupRRsigRecords(map<string, ProbeResult> rrsigProbeResult, long domainId) {
    MysqlClient mysqlClient;
    map<string, map<string, list<DnsRR *>>> rrsigGroupMap;
    for (map<string, ProbeResult>::iterator probeResultIter = rrsigProbeResult.begin();
         probeResultIter != rrsigProbeResult.end(); probeResultIter++) {
        map<uint16_t, long> keymap;
        string rrsigRecords = "";
        string rrsigRelationships = "";
        std::map<string, list<DnsRR *> > rrsigMap;
        for (ProbeInfo *info: probeResultIter->second.probeInfo) {

            for (Measurement *m: info->measurement) {

                RRList_t rrsigList;
                list<DnsRR *> listOfRRSigs;
                m->result->getAnswers(rrsigList);
                for (RRIter_t iter = rrsigList.begin();
                     rrsigList.end() != iter;
                     iter++) {


                    DnsRrsig *rrsig;
                    rrsig = dynamic_cast<DnsRrsig *>(*iter);
                    if (rrsig->getTypeCovered() == 48) {
//                    std::cout <<">>>>"<< m->subid;

//timestamp, ttl, inception,expiration, keytag, signerName, signature, algorithm, labels

                        if (keymap.find(rrsig->getKeyTag()) == keymap.end()) {
                            rrsig->setRRSigId(++nextRRsigId);
                            rrsigRecords +=
                                    "('" + to_string(time(nullptr)) + "','" +
                                    to_string(rrsig->ttl()) + "','" + to_string(rrsig->getInception())
                                    + "','" + to_string(rrsig->getExpiration()) + "','" +
                                    to_string(rrsig->getKeyTag()) + "','" + rrsig->getSignersName() + "','" +
                                    rrsig->getSig() + "','" + to_string(rrsig->getAlgo()) + "','" +
                                    to_string(rrsig->getLabels()) + "'),";
                            keymap.insert(pair<uint16_t, long>(rrsig->getKeyTag(), rrsig->getRRSigId()));
                        }


                        rrsigRelationships +=
                                "('" + to_string(std::time(nullptr)) + "','" + to_string(std::time(nullptr)) +"','" + to_string(info->probeid) + "','" +
                                to_string(m->subid) + "','" +
                                to_string(rrsig->getKeyTag()) + "','" + to_string(domainId)
                                + "'),";
//                    cout << "subid" << rrsig->getRRSigId();
                        listOfRRSigs.push_back(rrsig);
                    }
                }
                if (listOfRRSigs.size() > 0) {
                    string key = std::to_string(info->probeid) + "_" + to_string(m->subid);
                    key = m->destinationAddress;
                    if(rrsigMap.find(key) == rrsigMap.end())
                        rrsigMap.insert(pair<string, list<DnsRR *> >(key, listOfRRSigs));
                    else{
                        rrsigMap.find(key)->second.insert(rrsigMap.find(key)->second.end(),listOfRRSigs.begin(), listOfRRSigs.end());
                    }

                }

            }
        }
        if (!rrsigRelationships.empty() && !rrsigRecords.empty()) {
//            mysqlClient.insertRRSIGRecords(rrsigRecords, rrsigRelationships);
        }

        rrsigGroupMap.insert(pair<string, map<string, list<DnsRR *>>>(probeResultIter->first, rrsigMap));
    }

    return rrsigGroupMap;
}

map<string, DnssecRRset *>
Evaluation::groupRecordsByProbe(map<string, list<DnsDnskey *>> dnskeyGroup, map<string, list<DnsRR *>> rrsigGroup) {
    map<string, DnssecRRset *> dnssecGroupByProbes;

    map<string, list<DnsDnskey *>>::iterator dnskeyGroupIter;
    for (dnskeyGroupIter = dnskeyGroup.begin(); dnskeyGroupIter != dnskeyGroup.end(); dnskeyGroupIter++) {

        list<DnsDnskey *> dnskeyList;
        cout << dnskeyGroupIter->first << " :\n";
//        cout << dnskeyGroupIter->second.size();
        for (DnsDnskey *dnskey: dnskeyGroupIter->second) {

            dnskeyList.push_back(dnskey);
        }
        list<DnsRR *>::iterator rrsigIter;
        list<DnsRR *> rrsigList;
        if (rrsigGroup.find(dnskeyGroupIter->first) != rrsigGroup.end()) {
            for (rrsigIter = rrsigGroup.find(dnskeyGroupIter->first)->second.begin();
                 rrsigIter != rrsigGroup.find(dnskeyGroupIter->first)->second.end(); rrsigIter++) {
                rrsigList.push_back((*rrsigIter));
            }
        }
        DnssecRRset *dnssecRRset;
        int end;
        unsigned long probeIdLength = dnskeyGroupIter->first.find('_', end);
        dnssecRRset->probeId = stol(dnskeyGroupIter->first.substr(0, probeIdLength - 0));
        dnssecRRset->subId = stol(
                dnskeyGroupIter->first.substr(probeIdLength + 1, dnskeyGroupIter->first.length() - 1));
        dnssecRRset->dnskeyList = dnskeyList;
        dnssecRRset->rrsigList = rrsigList;

        dnssecGroupByProbes.insert(pair<string, DnssecRRset *>(dnskeyGroupIter->first, dnssecRRset));


    }

    return dnssecGroupByProbes;
}

list<DnssecMeasurement>
Evaluation::verifyKeys(list<DnsDnskey *> dnskeyList, list<DnsRrsig *> rrsigList, string probe_sub_id, long domainId) {
    list<DnssecMeasurement> dnssecMeasurementList;
    cout <<"hetereee";
//    map<long, bool> verifiedSigs;
MysqlClient mysqlClient;
cout << dnskeyList.size();

    for (DnsDnskey *dnskey: dnskeyList) {
        DnssecMeasurement dnssecMeasurement;
        int end;
        unsigned long probeIdLength = probe_sub_id.find('_', end);
        cout << "evaluate1";
        cout << " " << probe_sub_id;
        dnssecMeasurement.isKey = true;
        if(probeIdLength + 1 > probe_sub_id.size()){
        cout << "evaluate1";

        dnssecMeasurement.probeId = stol(probe_sub_id.substr(0, probeIdLength - 0));
        dnssecMeasurement.subId = stol(probe_sub_id.substr(probeIdLength + 1, probe_sub_id.length() - 1));}
//dnssecMeasurement.probeId = probe_sub_id;;
//        dnssecMeasurement.subId = probe_sub_id;

//        dnssecMeasurement.key = dnskey->getKey();
        dnssecMeasurement.dnskeyKeytag = dnskey->calcKeyTag();
        for (DnsRrsig *rrsig: rrsigList) {

            if (dnskey->calcKeyTag() == rrsig->getKeyTag()) {
                dnssecMeasurement.isVerified = true;
                dnssecMeasurement.sigkeyTag = rrsig->getKeyTag();
                dnssecMeasurement.signature = rrsig->getSig();
//                mysqlClient.insertDNSSECMeasurementRecords(dnssecMeasurement.probeId, dnskey->calcKeyTag(), dnssecMeasurement.subId, domainId);
//                verifiedSigs.insert(pair<long, bool>(rrsig->getKeyTag(), true));
            }

        }
        cout << "evaluate1";
        dnssecMeasurementList.push_back(dnssecMeasurement);
    }
    DnssecMeasurement dnssecMeasurement;
    for (DnsRrsig *rrsig: rrsigList) {

//        if (verifiedSigs.find(rrsig->getKeyTag()) == verifiedSigs.end()) {

        int end;
        unsigned long probeIdLength = probe_sub_id.find('_', end);
        cout << "evaluate2";
        cout << "\nprobeid sub id " << probe_sub_id;
//        dnssecMeasurement.probeId = probe_sub_id;
if(probeIdLength+1 > probe_sub_id.size()){
        dnssecMeasurement.probeId = stol(probe_sub_id.substr(0, probeIdLength - 0));
        dnssecMeasurement.subId = stol(probe_sub_id.substr(probeIdLength + 1, probe_sub_id.length() - 1));}
//        dnssecMeasurement.subId =probe_sub_id;
        cout << "evaluate2";
        dnssecMeasurement.isKey = false;
        dnssecMeasurement.isVerified = false;
        dnssecMeasurement.sigkeyTag = rrsig->getKeyTag();
        dnssecMeasurement.signature = rrsig->getSig();
//            verifiedSigs.insert(pair<long, bool>(rrsig->getKeyTag(), true));
        dnssecMeasurementList.push_back(dnssecMeasurement);
        cout << "evaluate2";

//        }

    }
    return dnssecMeasurementList;

}