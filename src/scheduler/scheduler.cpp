//
// Created by Priya Ravichander on 6/21/21.
//

#include <thread>
#include "../../include/scheduler.h"
#include "../../include/MysqlClient.h"
#include <chrono>

#include <iostream>

using namespace std;
using namespace std::chrono;

void Scheduler::initiateTimer() {
    std::thread schedulerThread;
    Scheduler scheduler;
    schedulerThread = std::thread(&Scheduler::scheduleTask, scheduler);
    schedulerThread.join();


}
void Scheduler::pollAndMeasureRecords(long dnskeyId, long rrsigId) {

}
void Scheduler::scheduleTaskForPollingDomains() {

    MysqlClient mysqlClient;
    list<tuple<string,string> > measurementIds = mysqlClient.fetchDomainRecords();
    std::thread measurementThread;

    for(tuple<string, string> i: measurementIds){
        cout << get<0>(i) << endl;
        cout << get<1>(i) << endl;
        Scheduler scheduler;
//        measurementThread = std::thread(&Scheduler::pollAndMeasureRecords, get<0>(i), get<1>(i), scheduler);

    }


}
void Scheduler::scheduleTask() {
    system_clock::time_point now = system_clock::now();
    auto s = duration_cast<seconds>(now.time_since_epoch());
    system_clock::time_point next_full_second = system_clock::time_point(++s);

    auto interval = seconds(30); // or milliseconds(500) or whatever
    auto wait_until = next_full_second;
    std::thread pollerThread;
    while (true)
    {
        this_thread::sleep_until(wait_until);
        cout << "rpinting ";
        Scheduler scheduler;
        pollerThread = std::thread(&Scheduler::scheduleTaskForPollingDomains, scheduler);

        pollerThread.join();
        wait_until += interval;
    }

}




