//
// Created by Priya Ravichander on 6/4/21.
//

#include "../../include/Validator.h"
#include <stdio.h>
#include <string>
#include <vector>

#include <iostream>
#include <nlohmann/json.hpp>
#include "../../include/dns/dns_packet.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_rrset.h"
#include "../../include/dns/i_dns_dupable.h"
#include "../../include/dns/dns_defs.h"
#include "../../include/proberesult.h"


using namespace std;

bool Validator::isValidInput(string domain) {
    if (domain.empty())
        return false;
    return true;
}


static const string base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";


static inline bool is_base64(unsigned char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
}

std::vector<unsigned char> Validator::base64_decode(string const &encoded_string) {
    int in_len = encoded_string.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::vector<unsigned char> ret;

    while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
        char_array_4[i++] = encoded_string[in_];
        in_++;
        if (i == 4) {
            for (i = 0; i < 4; i++)
                char_array_4[i] = base64_chars.find(char_array_4[i]);

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; (i < 3); i++)
                ret.push_back(char_array_3[i]);
            i = 0;
        }
    }

    if (i) {
        for (j = i; j < 4; j++)
            char_array_4[j] = 0;

        for (j = 0; j < 4; j++)
            char_array_4[j] = base64_chars.find(char_array_4[j]);

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
        char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

        for (j = 0; (j < i - 1); j++) {
            ret.push_back(char_array_3[j]);
        }
    }

    return ret;
}


DnsPacket *Validator::parseDecoding(string encodedString) {

    DnsPacket *dnsPacket = new DnsPacket();

    std::vector<unsigned char> vec = base64_decode(encodedString);

    u_char bArr[vec.size()];

//    cout << "\n vec size " << vec.size();
    int i = 0;
    for (vector<u_char>::iterator it = vec.begin();
         it != vec.end();
         it++) {
        bArr[i++] = *it;
    }

    dnsPacket->fromWire(bArr, vec.size());

//    cout << "\n" << vec.data();
    RRList_t l;
    dnsPacket->getAnswers(l);
//    cout << "\n" << l->getNumData() << "\n";

    RRList_t *list;

    return dnsPacket;
}

map<string, string> Validator::extractSOAData(map<string, string> resultJson) {
    map<string, string> result;
    for(map<string, string>::iterator iter = resultJson.begin(); iter!=resultJson.end(); iter++) {
        for (nlohmann::json res:nlohmann::json::parse(iter->second)) {

            DnsPacket *resPacket = new DnsPacket();

            if (!res["resultset"][0]["result"].empty()) {
                resPacket = parseDecoding(res["resultset"][0]["result"]["abuf"]);
                RRList_t answerList;
                resPacket->getAnswers(answerList);
                RRList_t authList;
                resPacket->getAuthority(authList);
                if (!answerList.empty()) {
                    for (RRIter_t iter1 = answerList.begin();
                         answerList.end() != iter1;
                         iter1++) {
                        if(!(*iter1)->getName().empty()){
                            result.insert(pair<string, string>(iter->first, (*iter1)->getName()));
                            continue;
                        }

                    }
                } else if (!authList.empty()) {
                    for (RRIter_t iter1 = authList.begin();
                         authList.end() != iter1;
                         iter1++) {

                        if(!(*iter1)->getName().empty()){
                            result.insert(pair<string, string>(iter->first, (*iter1)->getName()));

//                            cout << "\n" << result.find("www.verisign.com")->second;
                            continue;
                        }

                    }
                }

            }


        }
    }
    return result;
}

map<string, ProbeResult> Validator::extractRipeAtlasData(map<string, string> resultJson) {

    map<string, ProbeResult> dnskeyProbeResult;
    for(map<string,string>::iterator dnskeyResultIter = resultJson.begin();dnskeyResultIter!=resultJson.end();dnskeyResultIter++) {
        ProbeResult probeResult;
        for (nlohmann::json res:nlohmann::json::parse(dnskeyResultIter->second)) {
//        cout << res;

            ProbeInfo *probeInfo = new ProbeInfo();

//        cout << "\nhere>>>>> : " << res["prb_id"];
            probeInfo->probeid = res["prb_id"];

            probeInfo->measurementId = res["msm_id"];

            probeInfo->group_id = res["group_id"];

            probeInfo->timestamp = res["timestamp"];

            probeInfo->msm_name = res["msm_name"];
            probeInfo->from = res["from"];
            probeInfo->type = res["type"];

            probeInfo->storedTimestamp = res["stored_timestamp"];

            for (nlohmann::json msmt: res["resultset"]) {
//            cout <<"\n"<<msmt["result"];
                if (!msmt["result"].empty()) {
//            cout <<"\nbegin for";
                    DnsPacket *resPacket = new DnsPacket();
                    Measurement *measurement = new Measurement();
                    measurement->timestamp = msmt["time"];
                    measurement->lts = msmt["lts"];
                    measurement->subid = msmt["subid"];
                    measurement->submax = msmt["submax"];
                    measurement->destinationAddress = msmt["dst_addr"];
//                    measurement->destinationPort = msmt["dst_port"];
                    measurement->af = msmt["af"];
                    measurement->sourceAddress = msmt["src_addr"];
                    measurement->sourceAddress = msmt["src_addr"];
                    measurement->sourceAddress = msmt["src_addr"];
                    measurement->sourceAddress = msmt["src_addr"];
                    measurement->protocol = msmt["proto"];

                    measurement->rt = msmt["result"]["rt"];

                    measurement->abuf = msmt["result"]["abuf"];


                    resPacket = parseDecoding(measurement->abuf);

//            resPacket->print();

                    measurement->result = resPacket;



//                measurement->result->print();
                    probeInfo->measurement.push_back(measurement);
                }
//            cout <<"\nhereeeeee";
            }
            probeResult.probeInfo.push_back(probeInfo);
//        cout <<"\n yes";
        }
        dnskeyProbeResult.insert(pair<string, ProbeResult>(dnskeyResultIter->first, probeResult));
    }
    return dnskeyProbeResult;
}



