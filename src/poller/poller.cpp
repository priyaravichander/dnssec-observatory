//
// Created by Priya Ravichander on 6/4/21.
//



#include "../../include/MysqlClient.h"
#include "../../include/ripeatlashandler.h"
#include "../../include/Validator.h"
#include "../../include/dns/dns_rrset.h"
#include "../../include/evaluation.h"
#include "../../include/global_variables.h"
#include <iostream>
#include <unistd.h>
#include <ctime>
#include "../../include/poller.h"
#include <thread>
#include "../../include/scheduler.h"
#include <typeinfo>


std::string Poller::performPoll(string domain) {

    MysqlClient mysqlClient;
    mysqlClient.loadIds();


//    if (argc > 1 && "schedule" == argv[1]) {
//        Scheduler scheduler;
//        scheduler.initiateTimer();
//    }


//    string domain;
//    std::cout << "Please enter a domain : ";

//    std::cin >> domain;

    cout << "\n finding SOA records";
    RipeAtlasHandler ripeAtlasHandler;
long domainId;
    map<string, string> soaList;
    map<string, string> domainSoaMap;
    unsigned int microsecond = 1000000;
    Validator validator;
    map<string, string> domains;
    map<string, long> domainMeasurementIdMap;
    map<string, long> rrsigMeasurementIdMap;
    domains.insert(pair<string, string>(domain, domain));
    tuple<long,string, long, long> measurementIds = mysqlClient.getMeasurementIdsForDomain(domain);
    if (get<1>(measurementIds).empty()) {

        map<string, long> soaMeasurementIDs = ripeAtlasHandler.createRipeAtlasMeasurement(domains, "SOA", false);

        usleep(20 * microsecond);//sleeps for 3 second
        map<string, string> soaRipeAtlasResult = ripeAtlasHandler.getRipeAtlasMeasurement(soaMeasurementIDs);

        map<string, string> soaParsedResult = validator.extractSOAData(soaRipeAtlasResult);

        string soa;




        for (auto iter = soaParsedResult.begin(); iter != soaParsedResult.end(); iter++) {
            soa = soaParsedResult.find(iter->first)->second;

            DnsRRset rrset;
//            cout <<"soaParsedResult.probeInfo;";
            if (soa.empty()) {
                cout << "\nSOA not found for domain: " << domain;
                return "";
            }

            soaList.insert(pair<string, string>(soa, iter->first));
            domainSoaMap.insert(pair<string, string>(iter->first, soa));


        }

        cout << "\n SOA record found";
        measurementIds = mysqlClient.getMeasurementIdsForSOA( soa);
        if (get<1>(measurementIds).empty()) {
            domainMeasurementIdMap = ripeAtlasHandler.createRipeAtlasMeasurement(soaList, "DNSKEY", false);

//
            rrsigMeasurementIdMap = ripeAtlasHandler.createRipeAtlasMeasurement(soaList, "RRSIG", false);
            domainId = mysqlClient.insertDomainData(domainSoaMap, domainMeasurementIdMap, std::time(nullptr),
                                                    rrsigMeasurementIdMap);
            usleep(10 * microsecond);
        }else{
            domainId= get<0>(measurementIds);
//        cout << "here6" << "\n";
            soaList.insert(pair<string, string>(get<1>(measurementIds), domain));
            domainSoaMap.insert(pair<string, string>(domain, get<1>(measurementIds)));
            domainMeasurementIdMap.insert(pair<string, long>(domain, get<2>(measurementIds)));
            rrsigMeasurementIdMap.insert(pair<string, long>(domain, get<3>(measurementIds)));

        }

    } else {
        domainId= get<0>(measurementIds);
//        cout << "here6" << "\n";
        soaList.insert(pair<string, string>(get<1>(measurementIds), domain));
        domainSoaMap.insert(pair<string, string>(domain, get<1>(measurementIds)));
        domainMeasurementIdMap.insert(pair<string, long>(domain, get<2>(measurementIds)));
        rrsigMeasurementIdMap.insert(pair<string, long>(domain, get<3>(measurementIds)));

//        cout << "here7" << "\n";
    }


    cout << "\n Getting DNSKEY and RRSIG records";


    //


    map<string, string> dnskeyResult = ripeAtlasHandler.getRipeAtlasMeasurement(domainMeasurementIdMap);

    map<string, ProbeResult> dnskeyProbeResult = validator.extractRipeAtlasData(dnskeyResult);

    map<string, string> rrsigResult = ripeAtlasHandler.getRipeAtlasMeasurement(rrsigMeasurementIdMap);
    map<string, ProbeResult> rrsigProbeResult = validator.extractRipeAtlasData(rrsigResult);
    cout << "\n Measuring DNSKEYs\n";
    Evaluation eval;
    map<string, map<std::string, list<struct DnsDnskey *>>> dnskeyGroup = eval.groupDnskeyRecords(dnskeyProbeResult, domainId);
    map<string, map<std::string, list<struct DnsRR *>>> rrsigGroup = eval.groupRRsigRecords(rrsigProbeResult, domainId);
//
cout << "\n"<<"hererererererer";
//    map<string, DnssecRRset*> keyGroups = eval.groupRecordsByProbe(dnskeyGroup, rrsigGroup);

//
    map<string, map<string, map<string, list<DnssecMeasurement >>>> groupSignatureAndKeyByDomain;
    for (auto &domainDnskeyGroupIter : dnskeyGroup) {
        map<string, map<string, list<DnssecMeasurement >>> groupByKeyAndSignature;
        for (auto dnskeyGroupIter = domainDnskeyGroupIter.second.begin();
             dnskeyGroupIter != domainDnskeyGroupIter.second.end(); dnskeyGroupIter++) {

            list<string> keytags;
            list<DnsDnskey *> dnskeyList;
            list<DnsRrsig *> rrsigList;


//        cout << dnskeyGroupIter->second.size();
            for (DnsDnskey *dnskey: dnskeyGroupIter->second) {

                keytags.push_back("Key-" + to_string(dnskey->calcKeyTag()));
                dnskeyList.push_back(dnskey);
            }
            list<DnsRR *>::iterator rrsigIter;
            cout << "\nkjabsdjavsdjhvax";
            if (rrsigGroup.find(domainDnskeyGroupIter.first)->second.find(dnskeyGroupIter->first) !=
                rrsigGroup.find(domainDnskeyGroupIter.first)->second.end()) {
                cout << " rrsig";
                for (rrsigIter = rrsigGroup.find(domainDnskeyGroupIter.first)->second.find(
                        dnskeyGroupIter->first)->second.begin();
                     rrsigIter != rrsigGroup.find(domainDnskeyGroupIter.first)->second.find(
                             dnskeyGroupIter->first)->second.end(); rrsigIter++) {
                    keytags.push_back("Sig-" + to_string((dynamic_cast<DnsRrsig *>(*rrsigIter))->getKeyTag()));
                    rrsigList.push_back((dynamic_cast<DnsRrsig *>(*rrsigIter)));
                }
            }
            keytags.sort();
            std::string keyAndSigString;
            for (const string &keytag: keytags) {
                keyAndSigString += "|" + keytag;
            }
            cout << " kee kee";
            list<DnssecMeasurement> dnssecMeasurementList = eval.verifyKeys(dnskeyList, rrsigList,
                                                                            dnskeyGroupIter->first, domainId);

            cout << " adasdasdasdasdad";
            if (groupByKeyAndSignature.find(keyAndSigString) == groupByKeyAndSignature.end()) {

                map<string, list<DnssecMeasurement >> verifiedKeyMap;
                verifiedKeyMap.insert(
                        pair<string, list<DnssecMeasurement >>(dnskeyGroupIter->first, dnssecMeasurementList));
                groupByKeyAndSignature.insert(
                        pair<string, map<string, list<DnssecMeasurement >>>(keyAndSigString, verifiedKeyMap));
            } else {
                groupByKeyAndSignature.find(keyAndSigString)->second.insert(
                        pair<string, list<DnssecMeasurement >>(dnskeyGroupIter->first, dnssecMeasurementList));
            }

        cout << "\n"<<keyAndSigString;
        }
        cout << "jasbkjavsdjkv";
        groupSignatureAndKeyByDomain.insert(
                pair<string, map<string, map<string, list<DnssecMeasurement >>>>(domainDnskeyGroupIter.first,
                                                                                 groupByKeyAndSignature));
    }
    cout << "here10" << "\n";
    map<string, map<string, list<DnssecMeasurement >>>::iterator groupByKeyAndSignatureIter;
    nlohmann::json resultJson;
    for (auto &domainIter : groupSignatureAndKeyByDomain) {
        cout << "\ndomain: " << domainIter.first;
        nlohmann::json keytagGroupList;

        for (groupByKeyAndSignatureIter = domainIter.second.begin();
             groupByKeyAndSignatureIter != domainIter.second.end(); groupByKeyAndSignatureIter++) {
            nlohmann::json keytagGroupJson;
            cout << "\n" << groupByKeyAndSignatureIter->first << "\n";
            map<string, list<DnssecMeasurement >>::iterator innerMapIter;

            nlohmann::json measurementList;
            for (innerMapIter = groupByKeyAndSignatureIter->second.begin();
                 innerMapIter != groupByKeyAndSignatureIter->second.end(); innerMapIter++) {
                int end = 0;
//                unsigned long probeIdLength = innerMapIter->first.find('_', end);

                nlohmann::json probeInfo;
//                probeInfo["probe"] = probeMap.find(stol(innerMapIter->first.substr(0, probeIdLength - 0)))->second;
//                probeInfo["sub"] = stol(
//                        innerMapIter->first.substr(probeIdLength + 1, innerMapIter->first.length() - 1));
                nlohmann::json dnsRecordJsonList;
                nlohmann::json dnskeys;
                nlohmann::json rrsigs;
                nlohmann::json dnsRecordJson;
                for (auto listIter = innerMapIter->second.begin();
                     listIter != innerMapIter->second.end(); listIter++) {
                    if(listIter->sigkeyTag == -1)
                        dnskeys.push_back(listIter->dnskeyKeytag);
                    if(listIter->dnskeyKeytag == -1)
                        rrsigs.push_back(listIter->sigkeyTag);
                    dnsRecordJson["probeId"] = listIter->probeId;
                    dnsRecordJson["dnskeyKeyTag"] = listIter->dnskeyKeytag;
                    dnsRecordJson["rrsigKeyTag"] = listIter->sigkeyTag;
                    dnsRecordJson["isVerified"] = listIter->isVerified;
                    dnsRecordJson["signatures"] = rrsigs;
                    dnsRecordJson["dnskeys"] = dnskeys;

                }
                dnsRecordJsonList = dnsRecordJson;
                probeInfo["dnsRecords"] = dnsRecordJsonList;
                measurementList.push_back(probeInfo);
//                cout << "       Probe: "
//                     << probeMap.find(stol(innerMapIter->first.substr(0, probeIdLength - 0)))->second << " Sub: "
//                     << stol(
//                             innerMapIter->first.substr(probeIdLength + 1, innerMapIter->first.length() - 1)) << ";";
                cout << "       Probe: " << innerMapIter->first;


            }

            keytagGroupJson[groupByKeyAndSignatureIter->first] = measurementList;
            keytagGroupList.push_back(keytagGroupJson);
            list<DnssecMeasurement>::iterator listIter;

            for (listIter = groupByKeyAndSignatureIter->second.begin()->second.begin();
                 listIter != groupByKeyAndSignatureIter->second.begin()->second.end(); listIter++) {




                cout << "\n            Verified Keys: Probe Id: " << listIter->probeId << "   dns keytag:"
                     << listIter->dnskeyKeytag
                     << "      rrsig keytag: " << listIter->sigkeyTag << "     Is verified: "
                     << listIter->isVerified;

            }
        }

        resultJson[domainIter.first] = keytagGroupList;
    }
    return resultJson.dump();
}


