//
// Created by Priya Ravichander on 6/4/21.
//

#include "../../include/ripeatlashandler.h"
#include "../../include/rest_client.h"
#include <string>

#include <iostream>
#include <regex>
#include <nlohmann/json.hpp>
#include <fstream>
#include <list>

using json = nlohmann::json;
using namespace std;


map<string, long> RipeAtlasHandler::createRipeAtlasMeasurement(map<string, string> domainMap, string queryType, bool doFlag) {
    string url = "https://atlas.ripe.net/api/v2/measurements/?key=5ee41543-7b83-45f8-82d2-902d586e9019";

    std::ifstream f(
            "/Users/priyaravichander/gmu/RA/code/new2/dnssec-observatory/resources/measurementCreationRequest.json");
    json measurementCreation = json::parse(f);

    json definitions = measurementCreation["definitions"][0];
    measurementCreation["definitions"].clear();
    vector<string> soaList;
    for (map<string,string>::iterator iter =  domainMap.begin();iter!=domainMap.end();iter++) {
        definitions["query_argument"] = iter->first;
        definitions["query_type"] = queryType;
//    definitions["target"] = "8.8.8.8";
        definitions["set_do_bit"] = doFlag;
        soaList.push_back(iter->first);
        measurementCreation["definitions"].push_back(definitions);
    }

//    cout << measurementCreation;
    RESTClient restClient;
//    cout << measurementCreation.dump();
    string response = restClient.firePostRequest(url, measurementCreation.dump());

    json resultJson = json::parse(response);
    map<string, long> measurementIds;
    for (int i = 0; i < resultJson["measurements"].size(); i++) {
        measurementIds.insert(pair<string, long>(soaList[i], resultJson["measurements"][i]));
//        cout << resultJson["measurements"][i] << "\n";
    }

    return measurementIds;
}

map<string, string> RipeAtlasHandler::getRipeAtlasMeasurement(map<string, long> measurementIds) {
    string url = "https://atlas.ripe.net/api/v2/measurements/<measurementId>/results";
    RESTClient restClient;
    map<string, string> resultMap;
    for (map<string, long>::iterator iter = measurementIds.begin(); iter != measurementIds.end(); iter++) {
        url = std::regex_replace(url, std::regex("<measurementId>"), to_string(iter->second));
        resultMap.insert(pair<string, string>(iter->first, restClient.fireGetRequest(url)));
        cout <<"\n"<< resultMap.find(iter->first)->second;
    }

    return resultMap;


}