//
// Created by Priya Ravichander on 7/1/21.
//

#include <iostream>
#include "../../include/MysqlClient.h"
#include "../../include/poller.h"
#include "oatpp/network/Server.hpp"
#include "../../include/Configuration.hpp"
#include "../../include/Controller.hpp"

void run() {

    /* Register Components in scope of run() method */
    AppComponent components;

    /* Get router component */
    OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router);

    /* Create Controller and add all of its endpoints to router */
    auto myController = std::make_shared<Controller>();
    myController->addEndpointsToRouter(router);

    /* Get connection handler component */
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>, connectionHandler);

    /* Get connection provider component */
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>, connectionProvider);

    /* Create server which takes provided TCP connections and passes them to HTTP connection handler */
    oatpp::network::Server server(connectionProvider, connectionHandler);

    /* Priny info about server port */
    OATPP_LOGI("\nDnssec-Observatory", "Server running on port %s", connectionProvider->getProperty("port").getData());

    /* Run server */
    server.run();

}
int main(int argc, char** argv){
    MysqlClient mysqlClient;
    mysqlClient.loadIds();

//    if (argc > 1 && "schedule" == argv[1]) {
//        Scheduler scheduler;
//        scheduler.initiateTimer();
//    }


    oatpp::base::Environment::init();

    run();

    /* Print how much objects were created during app running, and what have left-probably leaked */
    /* Disable object counting for release builds using '-D OATPP_DISABLE_ENV_OBJECT_COUNTERS' flag for better performance */
    std::cout << "\nEnvironment:\n";
    std::cout << "objectsCount = " << oatpp::base::Environment::getObjectsCount() << "\n";
    std::cout << "objectsCreated = " << oatpp::base::Environment::getObjectsCreated() << "\n\n";

    oatpp::base::Environment::destroy();

}