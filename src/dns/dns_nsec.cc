/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#include <sstream>

#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_nsec.h"
#include "../../include/dns/dns_defs.h"
#include "../../include/dns/dns_compression.h"



using namespace std;

DnsNsec::DnsNsec()
  : DnsRR(DNS_RR_NSEC)
{
}

DnsNsec::~DnsNsec()
{

}

bool DnsNsec::operator==(const DnsRR &p_oRHS)
{
  bool bRet = DnsRR::operator==(p_oRHS);

  if (bRet)
  {
    DnsNsec &oRHS = (DnsNsec &) p_oRHS;

    bRet = getName() == oRHS.getName();

    bRet &= m_oTypes == oRHS.m_oTypes;
  }

  return bRet;
}

bool DnsNsec::operator==(const DnsNsec &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsNsec::operator==(DnsNsec &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsNsec &DnsNsec::operator=(const DnsRR &p_oRHS)
{
  const DnsNsec &oRHS = (const DnsNsec &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsNsec &DnsNsec::operator=(const DnsNsec &p_oRHS)
{
  DnsRR::operator=(p_oRHS);

  m_sName = p_oRHS.m_sName;
  m_oTypes = p_oRHS.m_oTypes;

  return *this;
}

bool DnsNsec::rdata_valid()
{
  return true;
}

std::string &DnsNsec::getNextName()
{
  return m_sName;
}

void DnsNsec::setNextName(std::string &p_sName)
{
  m_sName = p_sName;
  // set_rdata((u_char *) p_sName.c_str(), p_sName.size());
}

RRtypeIter_t DnsNsec::beginRRtypes()
{
  return m_oTypes.begin();
}

RRtypeIter_t DnsNsec::endRRtypes()
{
  return m_oTypes.end();
}

size_t DnsNsec::numRRtypes()
{
  return m_oTypes.size();
}

void DnsNsec::addRRtype(uint16_t p_uRRtype)
{
  m_oTypes.push_back(p_uRRtype);
}

bool DnsNsec::parseRData(u_char *p_pMsg,
                       size_t p_uMsgLen,
                       u_char *p_pRData,
                       size_t p_uRDataLen)
{
  bool bRet = false;

  m_oTypes.clear();
  set_rdata(p_pRData, p_uRDataLen);

  size_t uPreOffset =  p_pRData - p_pMsg;
  size_t uOffset =  p_pRData - p_pMsg;
  size_t uRdOffset = 0;
  DnsName *pName = DnsName::from_wire(p_pMsg, p_uMsgLen, uOffset);
  if (NULL == pName)
  {
    dns_log("Unable to DnsName::from_wire()\n");
  }
  else
  {
    uRdOffset = uOffset - uPreOffset;
    std::string sName = pName->toString();
    setNextName(sName);
    bRet = true;
    delete pName;

    dns_log("Offset %u Mgslen %u\n", (unsigned) uRdOffset, (unsigned) p_uMsgLen);
    // while (bRet && (uRdOffset + 2) < p_uMsgLen)
    while (bRet && (uRdOffset + 2) < p_uRDataLen)
    {
      uint8_t uBlock = (uint8_t) p_pRData[uRdOffset++];
      uint8_t uBitmapLen = (uint8_t) p_pRData[uRdOffset++];
      if ((uRdOffset + uBitmapLen) > p_uRDataLen)
      {
        dns_log("Unable to process NSEC record with block/bitmaplen greater than rdlen: %u > %u\n", 
                (unsigned) (uRdOffset + uBitmapLen),
                (unsigned) p_uRDataLen);
        //DnsError::getInstance().setError("Unable to process NSEC record with block/bitmaplen greater than rdlen");
        bRet = false;
      }
      else if (uBitmapLen > 32)
      {
        dns_log("Unable to process NSEC record with bitmaplen greater than 32: %u\n", 
                (unsigned) uBitmapLen);
        //DnsError::getInstance().setError("Unable to process NSEC record with bitmaplen greater than 32");
        bRet = false;
      }
      else
      {
        dns_log("<block, Bitmap len>: <%u, %u>\n", (unsigned) uBlock, (unsigned) uBitmapLen);
        uint8_t pBitmap[32];
        memcpy(pBitmap, &(p_pRData[uRdOffset]), uBitmapLen);
        uRdOffset += uBitmapLen;
        for (int i = 0; i < uBitmapLen; i++)
        {
          uint8_t uByte = pBitmap[i];
          dns_log("byte 0x%x\n", uByte);
          for (int j = 0; j < 8; j++)
          {
            uint8_t uTmpByte = uByte >> (7 - j);
            uTmpByte &= 0x01;
            if (uTmpByte)
            {
              uint16_t uRRtype = uBlock << 8 & 0xff00;
              uRRtype += j + (i * 8);
              // uRRtype = ntohs(uRRtype);
              addRRtype(uRRtype);
              dns_log("\tTYPE %u\n", (unsigned) uRRtype);
            }
          }
        }
      }
    }
  }

  return bRet;
}

DnsNsec *DnsNsec::dup()
{
  return new DnsNsec();
}

void DnsNsec::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
  /*
  for (RRtypeIter_t tIter = beginRRtypes();
       endRRtypes() != tIter;
       tIter++)
  {
    fprintf(stdout, "%u", (unsigned) *tIter);
  }
  */
}

std::string DnsNsec::toString()
{
  stringstream oSS;
  oSS << getNextName();
  for (RRtypeIter_t tIter = beginRRtypes();
      endRRtypes() != tIter;
      tIter++)
  {
    oSS << " " << (unsigned) *tIter;
  }
  return oSS.str();
}

bool DnsNsec::fromString(std::string &p_sRdata)
{
  bool bRet = false;

  char szName[256];
  memset(szName, 0, 256);

  bRet = (1 == sscanf(p_sRdata.c_str(), "%s", szName));
  if (bRet)
  {
    string sName = szName;
    setNextName(sName);
  }

  return bRet;
}

int DnsNsec::rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp)
{
  int iRet = -1;
  size_t uRdLen = get_rdlen();
  u_char *pRData = get_rdata();

  if (NULL == p_pBuff)
  {
    dns_log("Unable to write to NULL buffer.\n");
    //DnsError::getInstance().setError("Unable to write to NULL buffer.");
  }
  else if (uRdLen > 0 && NULL == pRData)
  {
    dns_log("RData ken is %u but buffer is NULL\n", (unsigned) uRdLen);
    //DnsError::getInstance().setError("RData ken is a # but buffer is NULL");
  }
  else
  {
    std::string sName = this->getName();
    DnsName *pName = new DnsName(sName);//pName should not be NULL right
    u_char *tmp_buffer = p_pBuff;
    p_pBuff += 2;
    p_uLen -= 2;
    if(pName)
    {
      u_char *pMsg = p_oComp.getMsg();
      size_t uOffset = (NULL != pMsg) ? p_pBuff - pMsg : p_uLen;
      int iNameLen = pName->toWire(p_pBuff, uOffset, p_oComp);
      if (-1 == iNameLen)
     {
       dns_log("Unable to init DnsNsec rdata name.\n");
       //DnsError::getInstance().setError("Unable to init DnsNsec rdata name.");
     }
     else
     {
       iRet = 2 + iNameLen;
       uint16_t uLen = htons(iNameLen);
       memcpy(tmp_buffer, &uLen, 2);
       p_pBuff += iNameLen;
       p_uLen -= iNameLen;
     }
     delete pName;
   }
   else//the ns name is NULl
   {
     tmp_buffer[0]=0;
     tmp_buffer[1]=0;
     iRet=2;
   }  
 }
 return iRet;
}

/*
int DnsNsec::verificationRData(DnsBits_t &p_oVec)
{
  int iRet = 0;

  u_char pBuff[255];
  memset(pBuff, 0, 255);
  string &sName = getName();
  DnsName oName(sName);
  iRet = oName.to_wire_canonical(pBuff, 255);

  DnsBits_t oLocal;

  for (int i = 0; i < iRet; i++)
  {
    oLocal.push_back(pBuff[i]);
  }

  uint16_t uRDLen = htons(iRet);
  p_oVec.push_back((htons(uRDLen) >> 8) & 0x00ff);
  p_oVec.push_back(htons(uRDLen) & 0x00ff);
  p_oVec.insert(p_oVec.end(), oLocal.begin(), oLocal.end());
  iRet+=2;
  dns_log("Canonical rdata length: %d\n", iRet);

  return iRet;
}
*/
