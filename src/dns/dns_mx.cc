/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */


#include "../../include/dns/config.h"
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#include <inttypes.h>

#include <sstream>

#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_mx.h"
#include "../../include/dns/dns_defs.h"
#include "../../include/dns/dns_compression.h"



using namespace std;

DnsMx::DnsMx()
  : DnsRR(DNS_RR_MX)
{
}

DnsMx::~DnsMx()
{

}

bool DnsMx::operator==(const DnsRR &p_oRHS)
{
  bool bRet = DnsRR::operator==(p_oRHS);

  if (bRet)
  {
    DnsMx &oRHS = (DnsMx &) p_oRHS;

    bRet = getName() == oRHS.getName();
  }

  return bRet;
}

bool DnsMx::operator==(const DnsMx &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsMx::operator==(DnsMx &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsMx &DnsMx::operator=(const DnsRR &p_oRHS)
{
  const DnsMx &oRHS = (const DnsMx &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsMx &DnsMx::operator=(const DnsMx &p_oRHS)
{
  DnsRR::operator=(p_oRHS);

  m_uPriority = p_oRHS.m_uPriority;
  m_sName = p_oRHS.m_sName;

  return *this;
}

bool DnsMx::rdata_valid()
{
  return true;
}

uint16_t DnsMx::getPriority()
{
  return m_uPriority;
}

void DnsMx::setPriority(uint16_t p_uPriority)
{
  m_uPriority = p_uPriority;
  u_char pBuff[257];
  memset(pBuff, 0, 257);

  uint16_t uTmp = htons(m_uPriority);
  memcpy(pBuff, &uTmp, sizeof(uint16_t));
  memcpy(pBuff + sizeof(uint16_t), (u_char *) m_sName.c_str(), m_sName.size());
  set_rdata(pBuff, m_sName.size() + sizeof(uint16_t));
}

std::string &DnsMx::getName()
{
  return m_sName;
}

void DnsMx::setName(std::string &p_sName)
{
  m_sName = p_sName;
  u_char pBuff[257];
  memset(pBuff, 0, 257);

  uint16_t uTmp = htons(m_uPriority);
  memcpy(pBuff, &uTmp, sizeof(uint16_t));
  memcpy(pBuff + sizeof(uint16_t), (u_char *) m_sName.c_str(), m_sName.size());
  set_rdata(pBuff, m_sName.size() + sizeof(uint16_t));
}

bool DnsMx::parseRData(u_char *p_pMsg,
                       size_t p_uMsgLen,
                       u_char *p_pRData,
                       size_t p_uRDataLen)
{
  bool bRet = false;

  if (!isQuestion())
  {
    set_rdata(p_pRData, p_uRDataLen);

    size_t uOffset =  (size_t) (p_pRData - p_pMsg);
    m_uPriority = ntohs(*((uint16_t *) &(p_pMsg[uOffset])));
    uOffset += sizeof(uint16_t);

    DnsName *pName = DnsName::from_wire(p_pMsg, p_uMsgLen, uOffset);
    if (NULL == pName)
    {
      dns_log("Unable to DnsName::from_wire()\n");
    }
    else
    {
      std::string sName = pName->toString();
      setName(sName);
      bRet = true;
      delete pName;
    }
  }

  return bRet;
}

DnsMx *DnsMx::dup()
{
  return new DnsMx();
}

void DnsMx::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

std::string DnsMx::toString()
{
  stringstream oSS;
  oSS << getPriority() << " " << getName();
  return oSS.str();
}

bool DnsMx::fromString(std::string &p_sRdata)
{
  bool bRet = false;

  char szName[256];
  memset(szName, 0, 256);

  bRet = (1 == sscanf(p_sRdata.c_str(), "%" SCNd16 " %s", &m_uPriority, szName));
  if (bRet)
  {
    string sName = szName;
    setName(sName);
  }

  return bRet;
}

int DnsMx::rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp)
{
  int iRet = -1;
  size_t uRdLen = get_rdlen();
  u_char *pRData = get_rdata();

  if (NULL == p_pBuff)
  {
    dns_log("Unable to write to NULL buffer.\n");
    //DnsError::getInstance().setError("Unable to write to NULL buffer.");
  }
  else if (uRdLen > 0 && NULL == pRData)
  {
    dns_log("RData ken is %u but buffer is NULL\n", (unsigned) uRdLen);
    //DnsError::getInstance().setError("RData ken is a # but buffer is NULL");
  }
  else
  {
    std::string sName = this->getName();
    DnsName *pName = new DnsName(sName);//pName should not be NULL right
    u_char *tmp_buffer = p_pBuff;
    p_pBuff += 2;
    p_uLen -= 2;

    *((uint16_t *) p_pBuff) = htons(m_uPriority);
    p_pBuff += sizeof(uint16_t);
    p_uLen -= sizeof(uint16_t);

    if(pName)
    {
      u_char *pMsg = p_oComp.getMsg();
      size_t uOffset = (NULL != pMsg) ? p_pBuff - pMsg : p_uLen;
      int iNameLen = pName->toWire(p_pBuff, uOffset, p_oComp);
      if (-1 == iNameLen)
     {
       dns_log("Unable to init DnsMx rdata name.\n");
       //DnsError::getInstance().setError("Unable to init DnsMx rdata name.");
     }
     else
     {
       iRet = 2 + sizeof(uint16_t) + iNameLen;
       // uint16_t uLen = htons(iNameLen);
       // uint16_t uLen = htons(iRet);
       uint16_t uLen = htons(iNameLen + sizeof(uint16_t));
       memcpy(tmp_buffer, &uLen, 2);
       p_pBuff += iNameLen;
       p_uLen -= iNameLen;
     }
     delete pName;
   }
   else//the ns name is NULl
   {
     tmp_buffer[0]=0;
     tmp_buffer[1]=0;
     iRet=2;
   }  
 }
 return iRet;
}

int DnsMx::verificationRData(DnsBits_t &p_oVec)
{
  int iRet = 0;

  u_char pBuff[255 + sizeof(uint16_t)];
  memset(pBuff, 0, 255 + sizeof(uint16_t));

  // (*(uint16_t *) pBuff) = htons(m_uPriority);
  uint16_t uNetPriority = htons(m_uPriority);
  memcpy(pBuff, &uNetPriority, sizeof(uNetPriority));
  iRet += sizeof(uint16_t);

  string &sName = getName();
  DnsName oName(sName);
  iRet += oName.to_wire_canonical(pBuff, 255);

  DnsBits_t oLocal;

  for (int i = 0; i < iRet; i++)
  {
    oLocal.push_back(pBuff[i]);
  }

  uint16_t uRDLen = htons(iRet);
  p_oVec.push_back((htons(uRDLen) >> 8) & 0x00ff);
  p_oVec.push_back(htons(uRDLen) & 0x00ff);
  p_oVec.insert(p_oVec.end(), oLocal.begin(), oLocal.end());
  iRet+=2;
  dns_log("Canonical rdata length: %d\n", iRet);

  return iRet;
}
