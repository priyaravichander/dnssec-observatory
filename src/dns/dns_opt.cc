/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>

#include <sstream>

#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_opt.h"
#include "../../include/dns/dns_defs.h"


using namespace std;

DnsOpt::DnsOpt(uint16_t p_uMax /*= 512*/, bool p_bDO /*= false*/)
  : DnsRR(DNS_RR_OPT)
{
  set_ttl(0);
  setMax(p_uMax);
  setDO(p_bDO);

  std::string sName = "";
  DnsName *pName = new DnsName(sName);
  init(pName, get_class(), ttl());
  delete pName;
}

DnsOpt::DnsOpt(DnsOpt &p_oRHS)
  : DnsRR(DNS_RR_OPT)
{
  *this = p_oRHS;
}

DnsOpt::~DnsOpt()
{
  clearOpts();
}

bool DnsOpt::operator==(const DnsRR &p_oRHS)
{
  bool bRet = DnsRR::operator==(p_oRHS);

  return bRet;
}

bool DnsOpt::operator==(const DnsOpt &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsOpt::operator==(DnsOpt &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsOpt &DnsOpt::operator=(const DnsRR &p_oRHS)
{
  const DnsOpt &oRHS = (const DnsOpt &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsOpt &DnsOpt::operator=(const DnsOpt &p_oRHS)
{
  DnsRR::operator=(p_oRHS);
  DnsOpt &oRHS = (DnsOpt &) p_oRHS;

  set_ttl(oRHS.ttl());
  setMax(oRHS.getMax());
  setDO(oRHS.getDO());

  clearOpts();
  for (OptIter_t tIter = oRHS.begin();
       oRHS.end() != tIter;
       tIter++)
  {
    const opt_code_t &tOpt = *tIter;
    addOption(tOpt.m_uCode, tOpt.m_uLen, tOpt.m_pValue);
  }

  return *this;
}

bool DnsOpt::rdata_valid()
{
  return (get_class() >= 512);
}

uint16_t DnsOpt::getMax()
{
  return get_class();
}

void DnsOpt::setMax(uint16_t p_uMax)
{
  set_class(p_uMax);
}

bool DnsOpt::getDO()
{
  // We need to handle this as it will be over
  // the wire.  So, since we call htonl later, we
  // need to be sure we set this in network byte order
  // and then convert it to host byte order so that
  // it will be converted back to netowrk byte order...
  // Are you ready for lunch yet?
  uint32_t uTTL = htonl(ttl());

  return (0 != (htonl(0x00008000) & uTTL));
}

void DnsOpt::setDO(bool p_bDO)
{
  // We need to handle this as it will be over
  // the wire.  So, since we call htonl later, we
  // need to be sure we set this in network byte order
  // and then convert it to host byte order so that
  // it will be converted back to netowrk byte order...
  // Are you ready for lunch yet?
  uint32_t uTTL = htonl(ttl());

  if (p_bDO)
  {
    uTTL |= htonl(0x00008000);
  }
  else
  {
    uTTL &= htonl(0xffff7fff);
  }
  set_ttl(ntohl(uTTL));
}

bool DnsOpt::isQuestion()
{
  return false;
}

DnsOpt *DnsOpt::dup()
{
  return new DnsOpt();
}

int DnsOpt::rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp)
{
  int iRet = -1;

  if (NULL == p_pBuff)
  {
    dns_log("Unable to convert to wire with NULL buffer.\n");
  }
  else
  {
    iRet = 0;
    uint16_t uRdLen = 0;

    u_char *pHead = p_pBuff;
    // FOr the RDLen
    p_pBuff += 2;

    for (OptIter_t tIter = begin();
         end() != tIter;
         tIter++)
    {
      // Copied on purpose so we can change the values in the
      // local var. but not the list var
      opt_code_t tCode = *tIter;

      int iLen = (int) tCode.m_uLen;
      // The length of this option code + <the code> + <the length>
      uRdLen += iLen + (2 * sizeof(uint16_t));

      tCode.m_uCode = htons(tCode.m_uCode);
      memcpy(p_pBuff, &tCode.m_uCode, sizeof(tCode.m_uCode));
      p_pBuff += sizeof(tCode.m_uCode);

      tCode.m_uLen = htons(tCode.m_uLen);
      memcpy(p_pBuff, &tCode.m_uLen, sizeof(tCode.m_uLen));
      p_pBuff += sizeof(tCode.m_uLen);

      memcpy(p_pBuff, tCode.m_pValue, iLen);
      p_pBuff += iLen;
    }

    iRet = (int) uRdLen + sizeof(uint16_t);
    uRdLen = htons(uRdLen);
    memcpy(pHead, (u_char *)&uRdLen, sizeof(uRdLen));
  }

  return iRet;
}

bool DnsOpt::deleteOption(uint16_t p_uCode)
{
   bool iRet=false;
   OptIter_t tIter;
   for(tIter = m_oOpts.begin(); tIter != m_oOpts.end(); tIter++)
   {
       if((*tIter).m_uCode == p_uCode)
       {
           delete[] (*tIter).m_pValue;
           m_oOpts.erase(tIter);
           iRet=true;
           break;
       }
   }
   return iRet;
}

bool DnsOpt::addOption(uint16_t p_uCode,
                       uint16_t p_uLength,
                       char *p_pValue)
{
  bool bRet = false;

  if (NULL == p_pValue)
  {
//    dns_log("Unable to add NULL option value.\n");
    opt_code_t tCode;
    memset(&tCode, 0, sizeof(tCode));

    tCode.m_uCode = p_uCode;
    tCode.m_uLen = p_uLength;
    tCode.m_pValue = NULL;
    m_oOpts.push_back(tCode);
    bRet = true;
  }
  else
  {
    opt_code_t tCode;
    memset(&tCode, 0, sizeof(tCode));

    tCode.m_uCode = p_uCode;
    tCode.m_uLen = p_uLength;
    if (p_uLength > 0)
    {
      tCode.m_pValue = new char[p_uLength];
      memcpy(tCode.m_pValue, p_pValue, p_uLength);
    }

    m_oOpts.push_back(tCode);

    bRet = true;
  }

  return bRet;
}

OptIter_t DnsOpt::begin()
{
  return m_oOpts.begin();
}

OptIter_t DnsOpt::end()
{
  return m_oOpts.end();
}

bool DnsOpt::clearOpts()
{
  bool bRet = true;

  for (OptIter_t tIter = begin();
       end() != tIter;
       tIter++)
  {
    delete[] (*tIter).m_pValue;
  }

  m_oOpts.clear();

  return bRet;
}


bool DnsOpt::parseRData(u_char *p_pMsg,
                            size_t p_uMsgLen,
                            u_char *p_pRData,
                            size_t p_uRDataLen)
{
      bool bRet = false;
      set_rdata(p_pRData, p_uRDataLen);
      u_char *buf=p_pRData;
      int rdlen = p_uRDataLen;
      if(rdlen > 2)
      {
          int rdoffset=0;
          unsigned short optlen;
          for (rdoffset = 0; rdoffset < rdlen-2; rdoffset += optlen + 4) 
          {
               bRet = true;
               unsigned short opcode;
               opcode = ntohs(*(unsigned short *)(buf + rdoffset));
               optlen = ntohs(*(unsigned short *)(buf + rdoffset+ 2));
               if (optlen + rdoffset + 2 > rdlen-2) 
               {
                   dns_log("optlen takes us out of the rdata");
                   //DnsError::getInstance().setError("optlen takes us out of the rdata");
                   bRet = false;
                   break;
               }
               if(optlen > 0)
               {
                   char * m_value= (char *)(buf + rdoffset + 4);
                   addOption(opcode, optlen, m_value);
               }
               else addOption(opcode, 0, NULL);
         }//end for
      }
      return bRet;
}





void DnsOpt::printRData()
{
  fprintf(stdout, "EDNS: version 0, flags: ");
  if (getDO())
  {
    fprintf(stdout, " do");
  }
  fprintf(stdout, "; udp: %u\n", getMax());
  for (OptIter_t tIter = begin();
       end() != tIter;
       tIter++)
  {
    opt_code_t tCode = *tIter;
    fprintf(stdout, "  < 0%x, %u, ", tCode.m_uCode, tCode.m_uLen);
    for (int i = 0; i < tCode.m_uLen; i++)
    {
      fprintf(stdout, "0x%x ", (0x00ff & tCode.m_pValue[i]));
    }
    fprintf(stdout, " >\n");
  }
}

std::string DnsOpt::toString()
{
  stringstream oSS;

  oSS << getDO() 
      << " " << getMax();

  for (OptIter_t tIter = begin();
       end() != tIter;
       tIter++)
  {
    opt_code_t tCode = *tIter;
    oSS << "  < " << tCode.m_uCode << ", " << tCode.m_uLen;
    for (int i = 0; i < tCode.m_uLen; i++)
    {
      oSS << (0x00ff & tCode.m_pValue[i]) << " ";
    }
    oSS << " > :: ";
  }

  return oSS.str();
}

int DnsOpt::setFlag(uint8_t p_uPos, bool p_bVal)
{
  int iRet = -1;

  if(p_uPos > 15){
    return iRet;
  }

  uint32_t uTTL = htonl(ttl());

  if(p_bVal)
  {
    uTTL |= htonl(0x00008000 >> p_uPos);
  }
  else
  {
    uTTL &= htonl(~(0x00008000 >> p_uPos));
  }

  set_ttl(ntohl(uTTL));

  iRet = p_uPos;
  return iRet;
}

void DnsOpt::setVersion(uint8_t p_uVer)
{
  uint32_t uTTL = htonl(ttl());
  uint32_t uTmp = p_uVer << 16;

  uTTL &= htonl(0xff00ffff);
  uTTL |= uTmp;

  set_ttl(ntohl(uTTL));
}

void DnsOpt::setRcode(uint8_t p_uVer)
{
  uint32_t uTTL = htonl(ttl());
  uint32_t uTmp = p_uVer << 24;

  uTTL &= htonl(0x00ffffff);
  uTTL |= uTmp;

  set_ttl(ntohl(uTTL));
}
