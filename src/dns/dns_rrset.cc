/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <stdio.h>
#include <stdlib.h>

#include "../../include/dns/dns_rrset.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_rrsig.h"

using namespace std;

DnsRRset::DnsRRset()
  : m_bValid(false),
    m_iClass(-1),
    m_iType(-1)
{

}

DnsRRset::DnsRRset(RRList_t &p_oRRs)
  : m_bValid(false),
    m_iClass(-1),
    m_iType(-1)
{
  if (!init(p_oRRs))
  {
    dns_log("Unable to construct valid RRset from RR list.\n");
  }
}

DnsRRset::DnsRRset(const DnsRRset &p_oRHS)
  : m_bValid(false),
    m_iClass(-1),
    m_iType(-1)
{
  *this = p_oRHS;
}

DnsRRset::~DnsRRset()
{
  clear();
}

const DnsRRset &DnsRRset::operator=(const DnsRRset &p_oRHS)
{
  clear();

  m_iType = p_oRHS.m_iType;
  m_iClass = p_oRHS.m_iClass;
  m_sName = p_oRHS.m_sName;
  m_bValid = p_oRHS.m_bValid;

  p_oRHS.getData(m_oDataRRs);
  p_oRHS.getSigs(m_oSigRRs);

  return *this;
}

bool DnsRRset::operator==(const DnsRRset &p_oRHS)
{
  bool bRet = false;

  DnsRRset &oRHS = (DnsRRset &) p_oRHS;
  if (equiv(oRHS))
  {
    dns_log("Sets are equiv... but are the RRSIGs equal?\n");
    RRIter_t tIter;
    list<string> oSigList;
    for (tIter = beginSigs();
         endSigs() != tIter;
         tIter++)
    {
      oSigList.push_back((*tIter)->toString());
      // cerr << "RRSIG: " << (*tIter)->toString() << endl;
    }
    oSigList.sort();

    list<string> oRhsSigList;
    for (tIter = oRHS.beginSigs();
         oRHS.endSigs() != tIter;
         tIter++)
    {
      oRhsSigList.push_back((*tIter)->toString());
      // cerr << "RHS RRSIG: " << (*tIter)->toString() << endl;
    }
    oRhsSigList.sort();

    bRet = (oSigList == oRhsSigList);
    dns_log("RRSIGs equal? %d\n", bRet);
  }

  return bRet;
}

bool DnsRRset::equiv(DnsRRset &p_oRHS)
{
  bool bRet = getType() == p_oRHS.getType()
              && getClass() == p_oRHS.getClass()
              && getName() == p_oRHS.getName();

  if (!bRet)
  {
    dns_log("Not even the basics are equiv...\n");
  }
  else
  {
    RRIter_t tIter;
    list<string> oDataList;
    for (tIter = beginData();
         endData() != tIter;
         tIter++)
    {
      oDataList.push_back((*tIter)->toString());
      // cerr << "DATA: " << (*tIter)->toString() << endl;
    }
    oDataList.sort();

    list<string> oRhsDataList;
    for (tIter = p_oRHS.beginData();
         p_oRHS.endData() != tIter;
         tIter++)
    {
      oRhsDataList.push_back((*tIter)->toString());
      // cerr << "RHS DATA: " << (*tIter)->toString() << endl;
    }
    oRhsDataList.sort();

    bRet = (oDataList == oRhsDataList);
    dns_log("Are the data RRs the same? %d\n", bRet);
  }

  return bRet;
}

bool DnsRRset::init(RRList_t &p_oRRs)
{
  int iClass = -1;
  int iType = -1;
  string sName;

  // DnsRrFactory &oFac = DnsRrFactory::getInstance();
  for (RRIter_t tIter = p_oRRs.begin();
       p_oRRs.end() != tIter;
       tIter++)
  {
    DnsRR *pRR = *tIter;
    if (NULL == pRR)
    {
      dns_log("Found NULL RR in list.");
      iClass = -1;
      iType = -1;
      break;
    }
    else
    {
      DnsName *pName = pRR->get_name();
      if (NULL == pName)
      {
        dns_log("Unable to process RR with NULL name.\n");
        iClass = -1;
        iType = -1;
        break;
      }
      else if (-1 == iClass)
      {
        if (DNS_RR_RRSIG == pRR->type())
        {
          // DnsRrsig *pSig = static_cast<DnsRrsig *>(oFac.create(oFac.getProtoNum("RRSIG")));
          DnsRrsig *pSig = new DnsRrsig(*static_cast<DnsRrsig *>(pRR));
          m_oSigRRs.push_back(pSig);
        }
        else
        {
          iClass = pRR->get_class();
          iType = pRR->type();
          sName = pName->toString();

          // DnsRR *pData = static_cast<DnsRR *>(oFac.create(iType));
          DnsRR *pData = pRR->dup();
          *pData = *pRR;
          m_oDataRRs.push_back(pData);
        }
      }
      else if (sName != pName->toString())
      {
        dns_log("Found RR with mismatched name: '%s' != '%s'\n", (pName->toString()).c_str(), sName.c_str());
        iClass = -1;
        iType = -1;
        break;
      }
      else
      {
        if (DNS_RR_RRSIG == pRR->type())
        {
          // DnsRrsig *pSig = static_cast<DnsRrsig *>(oFac.create(oFac.getProtoNum("RRSIG")));
          DnsRrsig *pSig = new DnsRrsig(*static_cast<DnsRrsig *>(pRR));
          m_oSigRRs.push_back(pSig);
        }
        else
        {
          iClass = pRR->get_class();
          iType = pRR->type();
          sName = pName->toString();

          // DnsRR *pData = static_cast<DnsRR *>(oFac.create(iType));
          DnsRR *pData = pRR->dup();
#ifdef _DNS_DEBUG
          dns_log("Calling copy constructor on type %d:\n", pRR->type());
          pRR->printRData();
          fprintf(stdout, "\n");
#endif
          (*pData) = *pRR;
#ifdef _DNS_DEBUG
          dns_log("RESULTS:::::::::::::::\n");
          pData->printRData();
          fprintf(stdout, "\n");
#endif
          m_oDataRRs.push_back(pData);
        }
      }
    }
  }

  if (-1 == iClass
      || -1 == iType)
  {
    clear();
  }
  else
  {
    m_iClass = iClass;
    m_iType = iType;
    m_sName = sName;
    m_bValid = true;
  }

  return m_bValid;
}

bool DnsRRset::isValid()
{
  return m_bValid;
}

int DnsRRset::getClass()
{
  return m_iClass;
}

int DnsRRset::getType()
{
  return m_iType;
}

const std::string &DnsRRset::getName()
{
  return m_sName;
}

RRIter_t DnsRRset::beginData()
{
  return m_oDataRRs.begin();
}

RRIter_t DnsRRset::endData()
{
  return m_oDataRRs.end();
}

size_t DnsRRset::getNumData()
{
  return m_oDataRRs.size();
}

bool DnsRRset::getData(RRList_t &p_oRRs) const
{
  for (KRRIter_t tIter = m_oDataRRs.begin();
       m_oDataRRs.end() != tIter;
       tIter++)
  {
    DnsRR *pRR = *tIter;
    DnsRR *pNewRR = pRR->dup();
    *pNewRR = *pRR;
    p_oRRs.push_back(pNewRR);
  }

  return true;
}

RRIter_t DnsRRset::beginSigs()
{
  return m_oSigRRs.begin();
}

RRIter_t DnsRRset::endSigs()
{
  return m_oSigRRs.end();
}

size_t DnsRRset::getNumSigs()
{
  return m_oSigRRs.size();
}

bool DnsRRset::getSigs(RRList_t &p_oRRs) const
{
  for (KRRIter_t tIter = m_oSigRRs.begin();
       m_oSigRRs.end() != tIter;
       tIter++)
  {
    DnsRR *pRR = *tIter;
    DnsRR *pNewRR = pRR->dup();
    *pNewRR = *pRR;
    p_oRRs.push_back(pNewRR);
  }

  return true;
}

bool DnsRRset::clear()
{
  bool bRet = false;

  RRIter_t tIter;
  for (tIter = beginData();
       endData() != tIter;
       tIter++)
  {
    delete *tIter;
  }
  m_oDataRRs.clear();

  for (tIter = beginSigs();
       endSigs() != tIter;
       tIter++)
  {
    delete *tIter;
  }
  m_oSigRRs.clear();
  bRet = true;

  return bRet;
}

