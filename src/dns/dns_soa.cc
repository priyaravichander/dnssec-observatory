/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>

#include <iostream>
#include <sstream>

#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_soa.h"
#include "../../include/dns/dns_defs.h"

#include "../../include/dns/dns_compression.h"

#define DNS_SOA_NUM_INT_FIELDS 5

using namespace std;

DnsSoa::DnsSoa()
  : DnsRR(DNS_RR_SOA),
    m_uSerial(0),
    m_uRefresh(0),
    m_uRetry(0),
    m_uExpire(0),
    m_uMinimum(0)
{

}

DnsSoa::DnsSoa(std::string &p_sMName,
               std::string &p_sRName,
               uint32_t p_uSerial,
               uint32_t p_uRefresh,
               uint32_t p_uRetry,
               uint32_t p_uExpire,
               uint32_t p_uMinimum)
  : DnsRR(DNS_RR_SOA),
    m_sMName(p_sMName),
    m_sRName(p_sRName),
    m_uSerial(0),
    m_uRefresh(0),
    m_uRetry(0),
    m_uExpire(0),
    m_uMinimum(0)
{

}

DnsSoa::DnsSoa(DnsSoa &p_oRHS)
  : DnsRR(DNS_RR_SOA),
    m_uSerial(0),
    m_uRefresh(0),
    m_uRetry(0),
    m_uExpire(0),
    m_uMinimum(0)
{
  *this = p_oRHS;
}

DnsSoa::~DnsSoa()
{

}

bool DnsSoa::operator==(const DnsRR &p_oRHS)
{
  bool bRet = DnsRR::operator==(p_oRHS);

  if (bRet)
  {
    DnsSoa &oRHS = (DnsSoa &) p_oRHS;

    bRet = getMName() == oRHS.getMName()
           && getRName() == oRHS.getRName()
           && getSerial() == oRHS.getSerial()
           && getRefresh() == oRHS.getRefresh()
           && getRetry() == oRHS.getRetry()
           && getExpire() == oRHS.getExpire()
           && getMinimum() == oRHS.getMinimum();
  }
  else
  {
    dns_log("DnsSoa::operator==() failed in the DNsRR\n");
  }

  return bRet;
}

bool DnsSoa::operator==(const DnsSoa &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsSoa::operator==(DnsSoa &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsSoa &DnsSoa::operator=(const DnsRR &p_oRHS)
{
  const DnsSoa &oRHS = (const DnsSoa &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsSoa &DnsSoa::operator=(const DnsSoa &p_oRHS)
{
  DnsRR::operator=(p_oRHS);
  DnsSoa &oRHS = (DnsSoa &) p_oRHS;

  setMName(oRHS.getMName());
  setRName(oRHS.getRName());
  setSerial(oRHS.getSerial());
  setRefresh(oRHS.getRefresh());
  setRetry(oRHS.getRetry());
  setExpire(oRHS.getExpire());
  setMinimum(oRHS.getMinimum());

  return *this;
}

bool DnsSoa::rdata_valid()
{
  return true;
}

std::string &DnsSoa::getMName()
{
  return m_sMName;
}

void DnsSoa::setMName(std::string &p_sMName)
{
  m_sMName = p_sMName;
}

std::string &DnsSoa::getRName()
{
  return m_sRName;
}

void DnsSoa::setRName(std::string &p_sRName)
{
  m_sRName = p_sRName;
}

uint32_t DnsSoa::getSerial()
{
  return m_uSerial;
}

void DnsSoa::setSerial(uint32_t p_uSerial)
{
  m_uSerial = p_uSerial;
}

uint32_t DnsSoa::getRefresh()
{
  return m_uRefresh;
}

void DnsSoa::setRefresh(uint32_t p_uRefresh)
{
  m_uRefresh = p_uRefresh;
}

uint32_t DnsSoa::getRetry()
{
  return m_uRetry;
}

void DnsSoa::setRetry(uint32_t p_uRetry)
{
  m_uRetry = p_uRetry;
}

uint32_t DnsSoa::getExpire()
{
  return m_uExpire;
}

void DnsSoa::setExpire(uint32_t p_uExpire)
{
  m_uExpire = p_uExpire;
}

uint32_t DnsSoa::getMinimum()
{
  return m_uMinimum;
}

void DnsSoa::setMinimum(uint32_t p_uMinimum)
{
  m_uMinimum = p_uMinimum;
}

bool DnsSoa::parseRData(u_char *p_pMsg,
                       size_t p_uMsgLen,
                       u_char *p_pRData,
                       size_t p_uRDataLen)
{
  bool bRet = false;

  size_t uOffset =  (size_t) (p_pRData - p_pMsg);
  DnsName *pMName = NULL;
  DnsName *pRName = NULL;

  if (isQuestion())
  {
    dns_log("Questions do not have RData...\n");
    bRet = true;
  }
  else if (NULL == (pMName = DnsName::from_wire(p_pMsg, p_uMsgLen, uOffset)))
  {
    dns_log("Unable to DnsName::from_wire() for MName\n");
  }
  else if (NULL == (pRName = DnsName::from_wire(p_pMsg, p_uMsgLen, uOffset)))
  {
    dns_log("Unable to DnsName::from_wire() for RName\n");
  }
  else
  {
    std::string sName = pMName->toString();
    setMName(sName);
    delete pMName;

    sName = pRName->toString();
    setRName(sName);
    delete pRName;

    u_char *pTmp = &(p_pMsg[uOffset]);
    setSerial(ntohl(*(uint32_t *) pTmp));
    pTmp += sizeof(uint32_t);

    setRefresh(ntohl(*(uint32_t *) pTmp));
    pTmp += sizeof(uint32_t);

    setRetry(ntohl(*(uint32_t *) pTmp));
    pTmp += sizeof(uint32_t);
    
    setExpire(ntohl(*(uint32_t *) pTmp));
    pTmp += sizeof(uint32_t);
    
    setMinimum(ntohl(*(uint32_t *) pTmp));
    pTmp += sizeof(uint32_t);

    dns_log("Initialized SOA rdata:\n");
    bRet = true;
  }

  return bRet;
}

DnsSoa *DnsSoa::dup()
{
  return new DnsSoa();
}

void DnsSoa::printRData()
{
  cout << toString();
}

std::string DnsSoa::toString()
{
  stringstream oSS;
  oSS << getMName()
      << " " << getRName()
      << " " << getSerial()
      << " " << getRefresh()
      << " " << getRetry()
      << " " << getExpire()
      << " " << getMinimum();

  return oSS.str();
}

bool DnsSoa::fromString(std::string &p_sRdata)
{
  bool bRet = false;

  char szRName[256];
  char szMName[256];
  uint32_t uSerial = 0;
  uint32_t uRefresh = 0;
  uint32_t uRetry = 0;
  uint32_t uExpire = 0;
  uint32_t uMin = 0;
  memset(szRName, 0, 256);
  memset(szMName, 0, 256);

  int iRet = sscanf(p_sRdata.c_str(), 
                    "%s %s %u %u %u %u %u",
                    szMName,
                    szRName,
                    &uSerial,
                    &uRefresh,
                    &uRetry,
                    &uExpire,
                    &uMin);

  dns_log("Got %d out of 7 tokens of SOA\n", iRet);

  bRet = (7 == iRet);
  if (bRet)
  {
    string sRName = szRName;
    string sMName = szMName;
    setRName(sRName);
    setMName(sMName);
    setSerial(uSerial);
    setRefresh(uRefresh);
    setRetry(uRetry);
    setExpire(uExpire);
    setMinimum(uMin);
  }

  return bRet;
}

int DnsSoa::verificationRData(DnsBits_t &p_oVec)
{
  int iRet = 0;

  u_char *pBuff = NULL;
  string &sMName = getMName();
  string &sRName = getRName();
  DnsName oMName(sMName);
  DnsName oRName(sRName);
  DnsBits_t oLocal;

  int iTmp;
  int iSize = sMName.size();
  pBuff = new u_char[iSize + 1];
  memset(pBuff, 0, iSize + 1);
  iTmp = oMName.to_wire_canonical(pBuff, iSize + 1);
  for (int i = 0; i < iTmp; i++)
  {
    oLocal.push_back(pBuff[i]);
  }
  iRet += iTmp;
  delete[] pBuff;

  iSize = sRName.size();
  pBuff = new u_char[iSize + 1];
  memset(pBuff, 0, iSize + 1);
  iTmp = oRName.to_wire_canonical(pBuff, iSize + 1);
  for (int i = 0; i < iTmp; i++)
  {
    oLocal.push_back(pBuff[i]);
  }
  iRet += iTmp;
  delete[] pBuff;

  iRet += 5 * sizeof(uint32_t);

  uint16_t uRDLen = htons(iRet);
  p_oVec.push_back((htons(uRDLen) >> 8) & 0x00ff);
  p_oVec.push_back(htons(uRDLen) & 0x00ff);

  p_oVec.insert(p_oVec.end(), oLocal.begin(), oLocal.end());
  uint32_t pVals[5];
  pVals[0] = getSerial();
  pVals[1] = getRefresh();
  pVals[2] = getRetry();
  pVals[3] = getExpire();
  pVals[4] = getMinimum();

  for (int i = 0; i < 5; i++)
  {
    uint32_t u = (pVals[i]);
    p_oVec.push_back((u >> 24) & 0x00ff);
    p_oVec.push_back((u >> 16) & 0x00ff);
    p_oVec.push_back((u >> 8) & 0x00ff);
    p_oVec.push_back(u & 0x00ff);
  }

  iRet+=2;
  dns_log("Canonical rdata length: %d\n", iRet);

#ifdef _DNS_DEBUG
  fprintf(stderr, "SOA VERIFICATION HERE HERE\n");
  for (DnsBitIter_t tBlah = p_oVec.begin();
       p_oVec.end() != tBlah;
       tBlah++)
  {
    fprintf(stdout, "0x%02x ", *tBlah);
  }
  fprintf(stdout, "\n");
#endif

  return iRet;
}

int DnsSoa::rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp)
{
  int iRet = -1;


  if (NULL == p_pBuff)
  {
    dns_log("Unable to write to NULL buffer.\n");
    //DnsError::getInstance().setError("Unable to write to NULL buffer.");
  }
  else if (p_uLen < 2)
  {
    dns_log("Unable to write to undersized buffer (%u).\n", (unsigned) p_uLen);
    //DnsError::getInstance().setError("Unable to write to undersized len buffer.");
  }
  else
  {
    DnsName *pMName = new DnsName(getMName());
    DnsName *pRName = new DnsName(getRName());

    u_char *pCurrent = p_pBuff + 2;
    p_uLen -= 2;

    u_char *pMsg = p_oComp.getMsg();
    size_t uOffset = (NULL != pMsg) ? pCurrent - pMsg : p_uLen;
    int iMNameLen = pMName->toWire(pCurrent, uOffset, p_oComp);
    int iRNameLen = 0;
    if (-1 == iMNameLen)
    {
      dns_log("Unable to init DnsSoa rdata M-name.\n");
      //DnsError::getInstance().setError("Unable to init DnsSoa rdata M-name.");
    }
    else if (-1 == (iRNameLen = pRName->toWire(&(pCurrent[iMNameLen]), uOffset + iMNameLen, p_oComp)))
    {
      dns_log("Unable to init DnsSoa rdata R-name.\n");
      //DnsError::getInstance().setError("Unable to init DnsSoa rdata R-name.");
    }
    else if ((p_uLen - iMNameLen - iRNameLen) < (DNS_SOA_NUM_INT_FIELDS * sizeof(uint32_t)))
    {
      dns_log("Unable to init DnsSoa not enough room left in buffer after names %u.\n", (unsigned) p_uLen);
      //DnsError::getInstance().setError("Unable to init DnsSoa not enough room left in buffer after names.");
    }
    else
    {
      p_uLen -= (iRNameLen + iMNameLen);
      pCurrent += (iRNameLen + iMNameLen);
      iRet = 2 + iMNameLen + iRNameLen;

      int i = 0;
      uint32_t pFields[DNS_SOA_NUM_INT_FIELDS];
      pFields[i++] = getSerial();
      pFields[i++] = getRefresh();
      pFields[i++] = getRetry();
      pFields[i++] = getExpire();
      pFields[i++] = getMinimum();

      for (i = 0; i < DNS_SOA_NUM_INT_FIELDS; i++)
      {
        uint32_t uTmp = htonl(pFields[i]);
        memcpy(pCurrent, &uTmp, sizeof(uTmp));
        p_uLen -= sizeof(uTmp);
        pCurrent += sizeof(uTmp);
        iRet += sizeof(uTmp);
      }
    }

    delete pMName;
    delete pRName;
    uint16_t uRdataLen = htons(iRet - 2);
    memcpy(p_pBuff, &uRdataLen, sizeof(uRdataLen));
  }

  return iRet;
}
