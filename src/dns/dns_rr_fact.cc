/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <stdio.h>
#include <algorithm>

#include "../../include/dns/dns_rr_fact.h"
#include "../../include/dns/dns_defs.h"
#include "../../include/dns/dns_a.h"
#include "../../include/dns/dns_ns.h"
#include "../../include/dns/dns_opt.h"
#include "../../include/dns/dns_dnskey.h"
#include "../../include/dns/dns_rrsig.h"
#include "../../include/dns/dns_ds.h"
#include "../../include/dns/dns_tsig.h"
#include "../../include/dns/dns_srv.h"
#include "../../include/dns/dns_txt.h"
#include "../../include/dns/dns_soa.h"
#include "../../include/dns/dns_aaaa.h"
#include "../../include/dns/dns_cname.h"
#include "../../include/dns/dns_nsec.h"
#include "../../include/dns/dns_tlsa.h"
#include "../../include/dns/dns_mx.h"

DnsRrFactory DnsRrFactory::s_oInstance;

DnsRrFactory::DnsRrFactory()
{
  reg(DNS_RR_A, *(new DnsA()));
  reg(DNS_RR_NS, *(new DnsNs()));
  reg(DNS_RR_SRV, *(new DnsSrv()));
  reg(DNS_RR_OPT, *(new DnsOpt()));
  reg(DNS_RR_RRSIG, *(new DnsRrsig()));
  reg(DNS_RR_DNSKEY, *(new DnsDnskey()));
  reg(DNS_RR_DS, *(new DnsDs()));
  reg(DNS_RR_TSIG, *(new DnsTsig()));
  reg(DNS_RR_TXT, *(new DnsTxt()));
  reg(DNS_RR_SOA, *(new DnsSoa()));
  reg(DNS_RR_AAAA, *(new DnsAaaa()));
  reg(DNS_RR_CNAME, *(new DnsCname()));
  reg(DNS_RR_NSEC, *(new DnsNsec()));
  reg(DNS_RR_TLSA, *(new DnsTlsa()));
  reg(DNS_RR_MX, *(new DnsMx()));

  //RFC 5395 20080710

  //TYPES
  m_oTypeNameToNum["A"] = 1;
  m_oTypeNameToNum["NS"] = 2;
  m_oTypeNameToNum["MD"] = 3;
  m_oTypeNameToNum["MF"] = 4;
  m_oTypeNameToNum["CNAME"] = 5;
  m_oTypeNameToNum["SOA"] = 6;
  m_oTypeNameToNum["MB"] = 7;
  m_oTypeNameToNum["MG"] = 8;
  m_oTypeNameToNum["MR"] = 9;
  m_oTypeNameToNum["NULL"] = 10;
  m_oTypeNameToNum["WKS"] = 11;
  m_oTypeNameToNum["PTR"] = 12;
  m_oTypeNameToNum["HINFO"] = 13;
  m_oTypeNameToNum["MINFO"] = 14;
  m_oTypeNameToNum["MX"] = 15;
  m_oTypeNameToNum["TXT"] = 16;
  m_oTypeNameToNum["RP"] = 17;
  m_oTypeNameToNum["AFSDB"] = 18 ;
  m_oTypeNameToNum["X25"] = 19;
  m_oTypeNameToNum["ISDN"] = 20;
  m_oTypeNameToNum["RT"] = 21;
  m_oTypeNameToNum["NSAP"] = 22;
  m_oTypeNameToNum["NSAP-PTR"] = 23;
  m_oTypeNameToNum["SIG"] = 24;
  m_oTypeNameToNum["KEY"] = 25;
  m_oTypeNameToNum["PX"] = 26;
  m_oTypeNameToNum["GPOS"] = 27;
  m_oTypeNameToNum["AAAA"] = 28;
  m_oTypeNameToNum["LOC"] = 29;
  m_oTypeNameToNum["NXT"] = 30;
  m_oTypeNameToNum["EID"] = 31;
  m_oTypeNameToNum["NIMLOC"] =32 ;
  m_oTypeNameToNum["SRV"] = 33;
  m_oTypeNameToNum["ATMA"] = 34;
  m_oTypeNameToNum["NAPTR"] = 35;
  m_oTypeNameToNum["KX"] = 36;
  m_oTypeNameToNum["CERT"] = 37;
  m_oTypeNameToNum["A6"] = 38;
  m_oTypeNameToNum["DNAME"] = 39 ;
  m_oTypeNameToNum["SINK"] = 40;
  m_oTypeNameToNum["OPT"] = 41;
  m_oTypeNameToNum["APL"] = 42;
  m_oTypeNameToNum["DS"] = 43;
  m_oTypeNameToNum["SSHFP"] = 44 ;
  m_oTypeNameToNum["IPSECKEY"] = 45 ;
  m_oTypeNameToNum["RRSIG"] = 46;
  m_oTypeNameToNum["NSEC"] = 47;
  m_oTypeNameToNum["DNSKEY"] = 48;
  m_oTypeNameToNum["DHCID"] = 49;
  m_oTypeNameToNum["NSEC3"] = 50;
  m_oTypeNameToNum["NSEC3PARAM"] = 51;
  m_oTypeNameToNum["TLSA"] = 52;
  m_oTypeNameToNum["HIP"] = 55;
  m_oTypeNameToNum["NINFO"] = 56;
  m_oTypeNameToNum["RKEY"] = 57;
  m_oTypeNameToNum["SPF"] = 99;
  m_oTypeNameToNum["UINFO"] = 100;
  m_oTypeNameToNum["UID"] = 101;
  m_oTypeNameToNum["GID"] = 102;
  m_oTypeNameToNum["UNSPEC"] = 103;
  m_oTypeNameToNum["TKEY"] = 249;
  m_oTypeNameToNum["TSIG"] = 250;
  m_oTypeNameToNum["IXFR"] = 251;
  m_oTypeNameToNum["AXFR"] = 252;
  m_oTypeNameToNum["MAILB"] = 253;
  m_oTypeNameToNum["MAILA"] = 254;
  m_oTypeNameToNum["TA"] = 32768;
  m_oTypeNameToNum["DLV"] = 32769;
  
  //CLASSES
  m_oClassNameToNum["IN"] = 1;
  m_oClassNameToNum["CH"] = 3;
  m_oClassNameToNum["HS"] = 4;
  m_oClassNameToNum["NONE"] = 254;
  m_oClassNameToNum["ANY"] = 255;
}

DnsRrFactory::~DnsRrFactory()
{
  reset();
}

DnsRrFactory &DnsRrFactory::getInstance()
{
  return s_oInstance;
}

DnsRR *DnsRrFactory::create(int p_iKey)
{
  DnsRR *pRet = DnsFactory<int, DnsRR>::create(p_iKey);

  if (NULL == pRet)
  {
    pRet = new DnsRR(p_iKey);
  }

  return pRet;
}

int DnsRrFactory::getProtoNum(std::string &p_sType)
{
  return getClassNum(p_sType);
}

int DnsRrFactory::getClassNum(std::string &p_sType)
{
  std::transform(p_sType.begin(), p_sType.end(), p_sType.begin(), ::toupper); 
  return m_oClassNameToNum[p_sType]; 
}

int DnsRrFactory::getTypeNum(std::string &p_sType)
{
  std::transform(p_sType.begin(), p_sType.end(), p_sType.begin(), ::toupper); 
  return m_oTypeNameToNum[p_sType];
}

