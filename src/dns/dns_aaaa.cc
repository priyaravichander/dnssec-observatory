/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <sstream>

#include "../../include/dns/dns_aaaa.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_defs.h"
#include <string.h>

using namespace std;

DnsAaaa::DnsAaaa()
  : DnsRR(DNS_RR_AAAA)
{
}

DnsAaaa::DnsAaaa(const DnsName &name, struct in6_addr &addr)
  :DnsRR(DNS_RR_AAAA)
{
    init((DnsName *) &name, DNS_CLASS_IN, 300, NULL, 0, (u_char *)&addr, 16);
}

bool DnsAaaa::rdata_valid()
{
  bool ret = true;
  if (m_rdlen != 16)
    ret = false;
  return ret;
}

void DnsAaaa::ip(struct in6_addr &p_tAddr)
{
   memset(&p_tAddr, 0, sizeof(p_tAddr));
   memcpy(&p_tAddr, m_rdata, sizeof(p_tAddr));
}

void DnsAaaa::set_ip(struct in6_addr &addr)
{
  set_rdata((u_char *)&addr, 16);
}


DnsAaaa *DnsAaaa::dup()
{
  return new DnsAaaa();
}

void DnsAaaa::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

std::string DnsAaaa::toString()
{
  char sz[INET6_ADDRSTRLEN] = {'\0'};

  inet_ntop(AF_INET6, (const char *)m_rdata, sz, sizeof(sz));
  string sRet = sz;
  return sRet;
}

