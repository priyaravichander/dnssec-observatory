/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sstream>
#include <iomanip>

#include "../../include/dns/dns_tlsa.h"
#include "../../include/dns/dns_rr.h"

#include "../../include/dns/dns_defs.h"
#include "../../include/dns/base64.h"


using namespace std;

DnsTlsa::DnsTlsa()
  : DnsRR(DNS_RR_TLSA),
    m_uUsage(0),
    m_uSelector(0),
    m_uMatching(0),
    m_iCADataLen(0),
    m_pCAData(NULL)
{
}

DnsTlsa::~DnsTlsa()
{
  setCertAssociationData(NULL, 0);
}

bool DnsTlsa::operator==(const DnsRR &p_oRHS)
{
  bool bRet = DnsRR::operator==(p_oRHS);

  if (bRet)
  {
    DnsTlsa &oRHS = (DnsTlsa &) p_oRHS;

    bRet = getUsage() == oRHS.getUsage();
    bRet &= getSelector() == oRHS.getSelector();
    bRet &= getMatching() == oRHS.getMatching();
    bRet &= getCertAssociationDataLen() == oRHS.getCertAssociationDataLen();
    if (bRet)
    {
      bRet &= (0 == memcmp(getCertAssociationData(), oRHS.getCertAssociationData(), getCertAssociationDataLen()));
    }
  }

  return bRet;
}

bool DnsTlsa::operator==(const DnsTlsa &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsTlsa::operator==(DnsTlsa &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsTlsa &DnsTlsa::operator=(const DnsRR &p_oRHS)
{
  const DnsTlsa &oRHS = (const DnsTlsa &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsTlsa &DnsTlsa::operator=(const DnsTlsa &p_oRHS)
{
  DnsRR::operator=(p_oRHS);

  DnsTlsa &oRHS = (DnsTlsa &) p_oRHS;
  setUsage(oRHS.getUsage());
  setSelector(oRHS.getSelector());
  setMatching(oRHS.getMatching());
  setCertAssociationData(oRHS.getCertAssociationData(), oRHS.getCertAssociationDataLen());

  return *this;
}

bool DnsTlsa::rdata_valid()
{
  return true;
}

uint8_t DnsTlsa::getUsage()
{
  return m_uUsage;
}

void DnsTlsa::setUsage(uint8_t p_uUsage)
{
  m_uUsage = p_uUsage;
}

uint8_t DnsTlsa::getSelector()
{
  return m_uSelector;
}

void DnsTlsa::setSelector(uint8_t p_uSelector)
{
  m_uSelector = p_uSelector;
}

uint8_t DnsTlsa::getMatching()
{
  return m_uMatching;
}

void DnsTlsa::setMatching(uint8_t p_uMatching)
{
  m_uMatching = p_uMatching;
}

u_char *DnsTlsa::getCertAssociationData()
{
  return m_pCAData;
}

int DnsTlsa::getCertAssociationDataLen()
{
  return m_iCADataLen;
}

void DnsTlsa::setCertAssociationData(u_char *p_pBuff, int p_iLen)
{
  if (NULL != m_pCAData)
  {
    delete[] m_pCAData;
    m_iCADataLen = 0;
  }

  if (NULL != p_pBuff && p_iLen > 0 && p_iLen <= SHRT_MAX)
  {
    m_iCADataLen = p_iLen;
    m_pCAData = new u_char[m_iCADataLen];
    memset(m_pCAData, 0, m_iCADataLen);
    memcpy(m_pCAData, p_pBuff, m_iCADataLen);
    //DnsError::getInstance().setError("");
  }
  else if (NULL != p_pBuff)
  {
    dns_log("Unable to DnsTlsa::setCertAssociationData with values Buff is NOT NULL and length is %d\n", p_iLen);
    //DnsError::getInstance().setError("Unable to DnsTlsa::setCertAssociationData with values Buff is NOT NULL and length as specified.");
  }
}

bool DnsTlsa::parseRData(u_char *p_pMsg,
                       size_t p_uMsgLen,
                       u_char *p_pRData,
                       size_t p_uRDataLen)
{
  bool bRet = false;

  if (!isQuestion() && p_uRDataLen > 0)
  {
    set_rdata(p_pRData, p_uRDataLen);

    setUsage(*(uint8_t *)p_pRData);
    p_pRData += sizeof(uint8_t);
    p_uRDataLen -= sizeof(uint8_t);

    setSelector(*(uint8_t *)p_pRData);
    p_pRData += sizeof(uint8_t);
    p_uRDataLen -= sizeof(uint8_t);

    setMatching(*(uint8_t *)p_pRData);
    p_pRData += sizeof(uint8_t);
    p_uRDataLen -= sizeof(uint8_t);

    setCertAssociationData((u_char *) p_pRData, p_uRDataLen);

    // std::string sKey = base64_encode((const unsigned char *) p_pRData, p_uRDataLen);
    // setKey(sKey);

    bRet = true;
  }

  return bRet;
}

DnsTlsa *DnsTlsa::dup()
{
  return new DnsTlsa();
}

void DnsTlsa::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

std::string DnsTlsa::toString()
{
  stringstream oSS;
  oSS << getName()
      << " " << (unsigned int) getUsage()
      << " " << (unsigned int) getSelector()
      << " " << (unsigned int) getMatching()
      << " " << getCertAssociationDataStr(); // base64_encode((const unsigned char *) getCertAssociationData(), getCertAssociationDataLen());

  return oSS.str();
}

bool DnsTlsa::fromString(std::string &p_sRdata)
{
  bool bRet = false;

  size_t uLen = p_sRdata.size();
  if (uLen > SHRT_MAX)
  {
    dns_log("Unable to parse oversized RR: %u\n", (unsigned) uLen);
    //DnsError::getInstance().setError("Unable to parse oversized RR");
  }
  else
  {
    uint8_t uUsage = 0;
    uint8_t uSelector = 0;
    uint8_t uMatching = 0;
    u_char *pBuff = new u_char[uLen];
    memset(pBuff, 0, uLen);

    bRet = (4 == sscanf(p_sRdata.c_str(), "%u %u %u %s", (unsigned *) &uUsage, (unsigned *) &uSelector, (unsigned *) &uMatching, pBuff));
    if (!bRet)
    {
      dns_log("Unable to scan TLSA data: %s\n", strerror(errno));
      //DnsError::getInstance().setError("Unable to scan TLSA data");
    }
    else
    {
      setUsage(uUsage);
      setSelector(uSelector);
      setMatching(uMatching);
      string sCAD = (const char *) pBuff;
      // setCertAssociationData(pBuff, uLen - 3);
      setCertAssociationDataStr(sCAD);
      //DnsError::getInstance().setError("No Error");
    }

    delete[] pBuff;
  }

  return bRet;
}

/*
int DnsTlsa::rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp)
{
  int iRet = -1;
  size_t uRdLen = get_rdlen();
  u_char *pRData = get_rdata();

  if (NULL == p_pBuff)
  {
    dns_log("Unable to write to NULL buffer.\n");
    //DnsError::getInstance().setError("Unable to write to NULL buffer.");
  }
  else if (uRdLen > 0 && NULL == pRData)
  {
    dns_log("RData len is %u but buffer is NULL\n", (unsigned) uRdLen);
    //DnsError::getInstance().setError("RData len is a # but buffer is NULL");
  }
  else if (uRdLen < 3 + getCertAssociationDataLen())
  {
    dns_log("RData len is %u too small < %u\n", (unsigned) uRdLen, (unsigned) 3 + getCertAssociationDataLen());
    //DnsError::getInstance().setError("RData len is too small");
  }
  else
  {
    u_char *pBuff = p_pBuff;
    uint8_t uUsage = getUsage();
    uint8_t uSelector = getSelector();
    uint8_t uMatching = getMatching();
    memcpy(pBuff, &uUsage, sizeof(uUsage));
    pBuff += sizeof(uUsage);

    memcpy(pBuff, &uSelector, sizeof(uSelector));
    pBuff += sizeof(uSelector);

    memcpy(pBuff, &uMatching, sizeof(uMatching));
    pBuff += sizeof(uMatching);

    memcpy(pBuff, getCertAssociationData(), getCertAssociationDataLen());

    iRet = getCertAssociationDataLen() + 3;
  }

  return iRet;
}

*/

bool DnsTlsa::setCertAssociationDataStr(std::string &p_sCAD)
{
  // vector<unsigned char> oVec = base64_decode(p_sCAD);
  /*
  static const char* const s_szConv = "0123456789ABCDEF";
  vector<unsigned char> oVec;
  size_t uLen = p_sCAD.size();
  oVec.reserve(2 * uLen);
  for (size_t u = 0; u < uLen; u++)
  {
    const unsigned char c = p_sCAD[u];
    oVec.push_back(s_szConv[c >> 4]);
    oVec.push_back(s_szConv[c & 15]);
  }
  */
  size_t uLen = p_sCAD.size();
  vector<unsigned char> oVec;
  for (size_t u = 0; u < uLen; u+=2)
  {
    string sByte = p_sCAD.substr(u, 2);
    unsigned char c = (unsigned char)(int) strtol(sByte.c_str(), NULL, 16);
    oVec.push_back(c);
  }

  m_sCAD = p_sCAD;
  setCertAssociationData(oVec.data(), oVec.size());
  return true;
}

std::string &DnsTlsa::getCertAssociationDataStr()
{
  /*
  m_sCAD = base64_encode(getCertAssociationData(), getCertAssociationDataLen());
  */
  stringstream oSS;
  oSS << setfill('0');
  const char *pBuff = (const char *) getCertAssociationData();
  size_t uLen = getCertAssociationDataLen();
  for (size_t u = 0; u < uLen; u++)
  {
    char sz[3] = {'\0', '\0', '\0'};
    sprintf(sz, "%02X", (uint8_t) pBuff[u]);
    oSS << setw(2) << sz;
  }
  m_sCAD = oSS.str();

  return m_sCAD;
}
