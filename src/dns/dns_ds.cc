/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and
 * Colorado State University All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "../../include/dns/config.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>

#include <openssl/ssl.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/md5.h>


#include <sstream>

#include "../../include/dns/dns_a.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/base64.h"
#include "../../include/dns/dns_ds.h"

#include "../../include/dns/dns_defs.h"

using namespace std;

DnsDs::DnsDs()
  : DnsRR(DNS_RR_DS),
    m_uKeyTag(0),
    m_uAlgo(0),
    m_uDigType(0),
    m_pBinDig(NULL),
    m_uBinDigLen(0)
{

}

DnsDs::DnsDs(DnsDs &p_oRHS)
  : DnsRR(DNS_RR_DS),
    m_uKeyTag(0),
    m_uAlgo(0),
    m_uDigType(0),
    m_pBinDig(NULL),
    m_uBinDigLen(0)
{
  *this = p_oRHS;
}

DnsDs::~DnsDs()
{
  setBinDig(NULL, 0);
}

bool DnsDs::operator==(const DnsRR &p_oRHS)
{
  // If the rdata is not set, then the
  // base class operator==() will not
  // work.
  // NOTE TODO : This may need to be fixed in
  // the future
  DnsDs &oRHS = (DnsDs &) p_oRHS;
  bool bRet = getKeyTag() == oRHS.getKeyTag()
         && getAlgo() == oRHS.getAlgo()
         && getDigType() == oRHS.getDigType()
         && getDig() == oRHS.getDig();

  return bRet;
}

bool DnsDs::operator==(const DnsDs &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

bool DnsDs::operator==(DnsDs &p_oRHS)
{
  return operator==((const DnsRR &) p_oRHS);
}

DnsDs &DnsDs::operator=(const DnsRR &p_oRHS)
{
  const DnsDs &oRHS = (const DnsDs &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsDs &DnsDs::operator=(const DnsDs &p_oRHS)
{
  DnsRR::operator=(p_oRHS);
  DnsDs &oRHS = (DnsDs &) p_oRHS;

  setKeyTag(oRHS.getKeyTag());
  setAlgo(oRHS.getAlgo());
  setDigType(oRHS.getDigType());
  setDig(oRHS.getDig());

  set_rdata(oRHS.get_rdata(), oRHS.get_rdlen());
  setBinDig(oRHS.getBinDig(), oRHS.getBinDigLen());

  return *this;
}

bool DnsDs::rdata_valid()
{
  return (NULL != getBinDig());
}

uint16_t DnsDs::getKeyTag()
{
  return m_uKeyTag;
}

void DnsDs::setKeyTag(uint16_t p_uKeyTag)
{
  m_uKeyTag = p_uKeyTag;
}

uint8_t DnsDs::getAlgo()
{
  return m_uAlgo;
}

void DnsDs::setAlgo(uint8_t p_uAlgo)
{
  m_uAlgo = p_uAlgo;
}

uint8_t DnsDs::getDigType()
{
  return m_uDigType;
}

void DnsDs::setDigType(uint8_t p_uDigType)
{
  m_uDigType = p_uDigType;
}

std::string &DnsDs::getDig()
{
  return m_sDig;
}

void DnsDs::setDig(std::string &p_sDig)
{
  m_sDig = p_sDig;
}

u_char *DnsDs::getBinDig()
{
  return m_pBinDig;
}

size_t DnsDs::getBinDigLen()
{
  return m_uBinDigLen;
}

void DnsDs::setBinDig(u_char *p_pDig, size_t p_uLen)
{
  if (NULL != m_pBinDig)
  {
    delete[] m_pBinDig;
    m_pBinDig = NULL;
  }

  if (NULL != p_pDig && p_uLen > 0)
  {
    m_pBinDig = new u_char[p_uLen];
    m_uBinDigLen = p_uLen;

    memcpy(m_pBinDig, p_pDig, m_uBinDigLen);
  }
}

bool DnsDs::parseRData(u_char *p_pMsg,
                           size_t p_uMsgLen,
                           u_char *p_pRData,
                           size_t p_uRDataLen)
{
  bool bRet = false;

  if (!isQuestion() && p_uRDataLen < 5)
  {
    dns_log("DS is not long enough: %u\n", (unsigned) p_uRDataLen);
  }
  else if (!isQuestion())
  {
    set_rdata(p_pRData, p_uRDataLen);

    setKeyTag(ntohs(*(uint16_t *)p_pRData));
    p_pRData += sizeof(uint16_t);
    p_uRDataLen -= sizeof(uint16_t);

    uint8_t uAlgo = (ntohs(*(uint16_t *) p_pRData) >> 8) & 0x00ff;
    setAlgo(uAlgo);

    uint8_t uDigType = ntohs(*(uint16_t *) p_pRData) & 0x00ff;
    setDigType(uDigType);

    p_pRData += sizeof(uint16_t);
    p_uRDataLen -= sizeof(uint16_t);

    setBinDig(p_pRData, p_uRDataLen);

    //this seems like a sloppy way to do this
    std::string sDig;
    char buf[3] = {'\0', '\0', '\0'};
    for (unsigned int i = 0; i < m_uBinDigLen; i++)
    {
      sprintf(buf, "%02X", m_pBinDig[i]);
      sDig += buf;
    }
    setDig(sDig);

    bRet = true;
  }

  return bRet;
}

DnsDs *DnsDs::dup()
{
  return new DnsDs();
}

void DnsDs::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

std::string DnsDs::toString()
{
  stringstream oSS;
  oSS << (unsigned) getKeyTag()
      << " " << (unsigned) getAlgo()
      << " " << (unsigned) getDigType()
      << " " <<  getDig().c_str();

  return oSS.str();
}

bool DnsDs::fromString(std::string &p_sRdata)
{
  bool bRet = false;

  char szDigest[256];
  unsigned uKeyTag = 0;
  unsigned uAlgo = 0;
  unsigned uDigType = 0;
  memset(szDigest, 0, 256);

  int iRet = sscanf(p_sRdata.c_str(),
                    "%u %u %u %s",
                    &uKeyTag,
                    &uAlgo,
                    &uDigType,
                    szDigest);

  dns_log("Got %d out of 4 tokens of SOA\n", iRet);

  bRet = (4 == iRet);
  if (bRet)
  {
    string sDigest = szDigest;
    setAlgo(uAlgo);
    setKeyTag(uKeyTag);
    setDigType(uDigType);
    setDig(sDigest);
  }

  return bRet;
}

bool DnsDs::init(uint8_t p_iDigType, DnsDnskey &p_oKey)
{
  bool bRet = false;
//
//  if (p_iDigType != DNS_DS_SHA1
//      && p_iDigType != DNS_DS_SHA256)
//  {
//    dns_log("Unable to init with unknown DS type %u (must be %u or %u)\n",
//            (unsigned) p_iDigType,
//            DNS_DS_SHA1,
//            DNS_DS_SHA256);
//    //DnsError::getInstance().setError("Unable to init with unknown DS type (must be 1 or 2)");
//  }
//  else
//  {
//    DnsBits_t oBits;
//    oBits.clear();
//
//    DnsName *pName = p_oKey.get_name();
//    u_char* pRData = p_oKey.get_rdata();
//
//    u_char pBuff[255];
//    memset(pBuff, 0, 255);
//    int iLen = pName->to_wire_canonical(pBuff, 255);
//
//    for (int i = 0; i < iLen; i++)
//    {
//      oBits.push_back(pBuff[i]);
//    }
//
//    for (size_t u = 0; u < p_oKey.get_rdlen(); u++)
//    {
//      oBits.push_back(pRData[u]);
//    }
//
//    unsigned int iDBLen = iLen + p_oKey.get_rdlen();
//    u_char dataBuff [iDBLen];
//    std::copy(oBits.begin(), oBits.end(), dataBuff);
//
//
//#ifdef _DNS_DEBUG
//    // Debugging info
//    printf("\nContents of the buffer to be hashed:\n");
//    for (int i = 0; i < iLen; i++)
//    {
//      if (dataBuff[i] < 65)
//        fprintf(stdout, "%d", dataBuff[i]);
//      else
//        fprintf(stdout,"%c", dataBuff[i]);
//    }
//
//    u_char* pRD = dataBuff + iLen;
//    size_t pRDLen = p_oKey.get_rdlen();
//    printf(" | %d", (ntohs(*(uint16_t *)pRD)));
//    pRD += sizeof(uint16_t);
//    pRDLen -= sizeof(uint16_t);
//
//    uint8_t uAlgo = (ntohs(*(uint16_t *) pRD) >> 8) & 0x00ff;
//    printf(" | %d", (uAlgo));
//
//    uint8_t uProto = ntohs(*(uint16_t *) pRD) & 0x00ff;
//    printf(" | %d", (uProto));
//
//    pRD += sizeof(uint16_t);
//    pRDLen -= sizeof(uint16_t);
//
//    std::string sKey = base64_encode((const unsigned char *) pRD, pRDLen);
//
//    printf(" | %s\n", sKey.c_str());//(p_pRData, p_uRDataLen));
//    //----
//#endif
//
//    int iErr = 0;
//
////    if (DNS_DS_SHA1 == p_iDigType)
////    {
////      SHA_CTX pShaCtx;
////      unsigned char pDigest[SHA_DIGEST_LENGTH];
////      unsigned char pRData[2 + 1 + 1 + SHA_DIGEST_LENGTH];
////      memset(&pShaCtx, 0, sizeof(pShaCtx));
////      memset(pDigest, 0, sizeof(pDigest));
////      memset(pRData, 0, sizeof(pRData));
////
////      uint16_t uKeyTag = p_oKey.calcKeyTag();
////      uKeyTag = htons(uKeyTag);
////      memcpy(pRData, &uKeyTag, sizeof(uKeyTag));
////      pRData[2] = (uint8_t) p_oKey.getAlgo();
////      pRData[3] = (uint8_t) p_iDigType;
//
////      if (1 != (iErr = SHA1_Init(&pShaCtx)))
////      {
////        dns_log("Unable to init sha1 context: '%s'\n", ERR_error_string(iErr, NULL));
////      }
////      else if (1 != (iErr = SHA1_Update(&pShaCtx, dataBuff, iDBLen)))
////      {
////        dns_log("Unable to create SHA1 hash: '%s'\n", ERR_error_string(iErr, NULL));
////      }
////      else if (1 != (iErr = SHA1_Final(pDigest, &pShaCtx)))
////      {
////        dns_log("Unable to finalize SHA1 context: '%s'\n", ERR_error_string(iErr, NULL));
////      }
////      else
////      {
////        setKeyTag(ntohs(uKeyTag));
////        setDigType(p_iDigType);
////        setAlgo(p_oKey.getAlgo());
////
////        string sDig;
////        char buf[3] = {'\0', '\0', '\0'};
////        for (unsigned int i = 0; i < SHA_DIGEST_LENGTH; i++)
////        {
////          sprintf(buf, "%02X", pDigest[i]);
////          sDig += buf;
////        }
////        setDig(sDig);
////        memcpy(&(pRData[4]), pDigest, SHA_DIGEST_LENGTH);
////        set_rdata(pRData, 2 + 1 + 1 + SHA_DIGEST_LENGTH);
////
////        bRet = true;
////      }
//    }
//    else if (DNS_DS_SHA256 == p_iDigType)
//    {
//      SHA256_CTX pShaCtx;
//      unsigned char pDigest[SHA256_DIGEST_LENGTH];
//      unsigned char pRData[2 + 1 + 1 + SHA256_DIGEST_LENGTH];
//      memset(&pShaCtx, 0, sizeof(pShaCtx));
//      memset(pDigest, 0, sizeof(pDigest));
//      memset(pRData, 0, sizeof(pRData));
//
//      uint16_t uKeyTag = p_oKey.calcKeyTag();
//      uKeyTag = htons(uKeyTag);
//      memcpy(pRData, &uKeyTag, sizeof(uKeyTag));
//      pRData[2] = (uint8_t) p_oKey.getAlgo();
//      pRData[3] = (uint8_t) p_iDigType;
//
//      if (1 != (iErr = SHA256_Init(&pShaCtx)))
//      {
//        dns_log("Unable to init sha1 context: '%s'\n", ERR_error_string(iErr, NULL));
//      }
//      else if (1 != (iErr = SHA256_Update(&pShaCtx, dataBuff, iDBLen)))
//      {
//        dns_log("Unable to create SHA256 hash: '%s'\n", ERR_error_string(iErr, NULL));
//      }
//      else if (1 != (iErr = SHA256_Final(pDigest, &pShaCtx)))
//      {
//        dns_log("Unable to finalize SHA256 context: '%s'\n", ERR_error_string(iErr, NULL));
//      }
//      else
//      {
//        setKeyTag(ntohs(uKeyTag));
//        setDigType(p_iDigType);
//        setAlgo(p_oKey.getAlgo());
//
//        string sDig;
//        char buf[3] = {'\0', '\0', '\0'};
//        for (unsigned int i = 0; i < SHA256_DIGEST_LENGTH; i++)
//        {
//          sprintf(buf, "%02X", pDigest[i]);
//          sDig += buf;
//        }
//        setDig(sDig);
//        memcpy(&(pRData[4]), pDigest, SHA256_DIGEST_LENGTH);
//        set_rdata(pRData, 2 + 1 + 1 + SHA256_DIGEST_LENGTH);
//
//        bRet = true;
//      }
//    }
//  }

  return bRet;
}
