/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"
#include <stdio.h>
#include <string.h>

#include <sstream>

#include "../../include/dns/dns_txt.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/dns_defs.h"


using namespace std;

DnsTxt::DnsTxt()
  : DnsRR(DNS_RR_TXT)
{
}

DnsTxt::DnsTxt(const DnsName &p_oName, std::string &p_sTxt)
  : DnsRR(DNS_RR_TXT)
{
  init(p_oName, p_sTxt);
}

DnsTxt::DnsTxt(const DnsName &p_oName, TxtList_t &p_oStrings)
  : DnsRR(DNS_RR_TXT)
{
  init(p_oName, p_oStrings);
}

bool DnsTxt::init(const DnsName &p_oName, std::string &p_sTxt)
{
  bool bRet = false;

  TxtList_t oList;
  int iLen = p_sTxt.length();
  // This is for the length bytes
  iLen += ((int) iLen / 255) + 1;
  if (strToList(p_sTxt, oList))
  {
    bRet = init(p_oName, oList, iLen);
  }

  return bRet;
}

bool DnsTxt::init(const DnsName &p_oName, TxtList_t &p_oStrings, int p_iLen /*= -1*/)
{
  bool bRet = true;

  if (-1 == p_iLen)
  {
    p_iLen = 0;
    for (TxtIter_t tIter = p_oStrings.begin();
         p_oStrings.end() != tIter;
         tIter++)
    {
      p_iLen += (*tIter).length() + 1;
    }
  }

  u_char *pBuff = new u_char[p_iLen];
  u_char *pTmp = pBuff;
  memset(pBuff, 0, p_iLen);
  for (TxtIter_t tIter = p_oStrings.begin();
       p_oStrings.end() != tIter;
       tIter++)
  {
    string &sChunk = *tIter;
    int iTmpLen = sChunk.length();
    if (iTmpLen > 255)
    {
      dns_log("Chunk is too big %d, aborting.\n", iTmpLen);
      //DnsError::getInstance().setError("Chunk is too big, aborting.");
      bRet = false;
      break;
    }
    else
    {
      uint8_t uTmpLen = (uint8_t) iTmpLen;
      pTmp[0] = uTmpLen;
      pTmp++;
      memcpy(pTmp, sChunk.c_str(), uTmpLen);
      pTmp += uTmpLen;
    }
  }

  if (bRet)
  {
    bRet = DnsRR::init(new DnsName(p_oName), DNS_CLASS_IN, 300, pBuff, p_iLen);
  }

  delete[] pBuff;

  return bRet;
}

bool DnsTxt::rdata_valid()
{
  bool ret = true;
  return ret;
}

std::string DnsTxt::toString()
{
  string sRet;

  u_char *pBuff = get_rdata();
  size_t uLen = get_rdlen();
  size_t u = 0;
  uint8_t uCount = 0;

  while (u < uLen)
  {
    uCount = (uint8_t) pBuff[u++];
    sRet.append((char *) &(pBuff[u]), (int) uCount);
    u += uCount;
  }

  return sRet;
}

bool DnsTxt::fromString(std::string &p_sRdata)
{
  DnsName *pName = get_name();
  return init(*pName, p_sRdata);
}

void DnsTxt::clear()
{
  set_rdata(NULL, 0);
}

DnsTxt *DnsTxt::dup()
{
  return new DnsTxt();
}

void DnsTxt::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

bool DnsTxt::strToList(std::string p_sString, TxtList_t &p_oOut)
{
  bool bRet = false;

  size_t uLen = p_sString.length();
  if (uLen >= 65535)
  {
    dns_log("Cannot encode length longer than a uint16_t can encode.\n");
    //DnsError::getInstance().setError("Cannot encode length longer than a uint16_t can encode.");
  }
  else
  {
    while (0 < uLen)
    {
      size_t uTmpLen = (uLen >= 255) ? 255 : uLen;
      string sChunk = p_sString.substr(0, uTmpLen);
      p_oOut.push_back(sChunk);
      p_sString.erase(0, uTmpLen);
      uLen = p_sString.length();
    }
    bRet = true;
  }

  return bRet;
}

bool DnsTxt::operator==(const DnsTxt &p_oRHS)
{
  return DnsRR::operator==((const DnsRR &) p_oRHS);
}

bool DnsTxt::operator==(DnsTxt &p_oRHS)
{
  return DnsRR::operator==((const DnsRR &) p_oRHS);
}

