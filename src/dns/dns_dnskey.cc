/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "../../include/dns/config.h"

#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>

#include <sstream>

#include "../../include/dns/dns_a.h"
#include "../../include/dns/dns_rr.h"
#include "../../include/dns/dns_name.h"
#include "../../include/dns/base64.h"
#include "../../include/dns/dns_dnskey.h"
#include "../../include/dns/dns_defs.h"

using namespace std;

DnsDnskey::DnsDnskey()
  : DnsRR(DNS_RR_DNSKEY),
    m_uFlags(0),
    m_uProto(0),
    m_uAlgo(0),
    m_pBinKey(NULL),
    m_uBinKeyLen(0)
{

}

DnsDnskey::DnsDnskey(DnsDnskey &p_oRHS)
  : DnsRR(DNS_RR_DNSKEY),
    m_uFlags(0),
    m_uProto(0),
    m_uAlgo(0),
    m_pBinKey(NULL),
    m_uBinKeyLen(0)
{
  *this = p_oRHS;
}

DnsDnskey::~DnsDnskey()
{
  setBinKey(NULL, 0);
}

DnsDnskey &DnsDnskey::operator=(const DnsRR &p_oRHS)
{
  const DnsDnskey &oRHS = (const DnsDnskey &) p_oRHS;
  *this = oRHS;

  return *this;
}

DnsDnskey &DnsDnskey::operator=(const DnsDnskey &p_oRHS)
{
  DnsRR::operator=(p_oRHS);
  dns_log("DnsDnskey::operator=()\n");
  DnsDnskey &oRHS = (DnsDnskey &) p_oRHS;

  setFlags(oRHS.getFlags());
  setProto(oRHS.getProto());
  setAlgo(oRHS.getAlgo());
  // Now setting this from within
  // setBinKey()
  /*
  setKey(oRHS.getKey());
  */

  set_rdata(oRHS.get_rdata(), oRHS.get_rdlen());
  setBinKey(oRHS.getBinKey(), oRHS.getBinKeyLen());

  return *this;
}

bool DnsDnskey::operator==(const DnsRR &p_oRHS)
{
  bool bRet = false;

  bRet = DnsRR::operator==(p_oRHS);
  if (bRet)
  {
    bRet = operator==((const DnsDnskey &) p_oRHS);
  }

  return bRet;
}

bool DnsDnskey::operator==(const DnsDnskey &p_oRHS)
{
  return operator==((DnsDnskey &) p_oRHS);
}

bool DnsDnskey::operator==(DnsDnskey &p_oRHS)
{
  bool bRet = false;

  bRet = getFlags() == p_oRHS.getFlags()
         && getProto() == p_oRHS.getProto()
         && getAlgo() == p_oRHS.getAlgo()
         && getKey() == p_oRHS.getKey();

  return bRet;
}

bool DnsDnskey::rdata_valid()
{
  // From the RFC, the protocol must be 3
  return (getProto() == 3);
}

uint16_t DnsDnskey::getFlags()
{
  return m_uFlags;
}

void DnsDnskey::setFlags(uint16_t p_uFlags)
{
  m_uFlags = p_uFlags;
}

uint8_t DnsDnskey::getProto()
{
  return m_uProto;
}

void DnsDnskey::setProto(uint8_t p_uProto)
{
  m_uProto = p_uProto;
}

uint8_t DnsDnskey::getAlgo()
{
  return m_uAlgo;
}

void DnsDnskey::setAlgo(uint8_t p_uAlgo)
{
  m_uAlgo = p_uAlgo;
}

std::string &DnsDnskey::getKey()
{
  return m_sKey;
}

void DnsDnskey::setKey(std::string &p_sKey)
{
  m_sKey = p_sKey;

  std::vector<unsigned char> oVec = base64_decode(m_sKey);
  dns_log("Decoded '%s' to bytes length %u\n", m_sKey.c_str(), (unsigned) oVec.size());
  setBinKey(oVec.data(), oVec.size());
}

u_char *DnsDnskey::getBinKey()
{
  return m_pBinKey;
}

size_t DnsDnskey::getBinKeyLen()
{
  return m_uBinKeyLen;
}

void DnsDnskey::setBinKey(u_char *p_pKey, size_t p_uLen)
{
  if (NULL != m_pBinKey)
  {
    delete[] m_pBinKey;
    m_pBinKey = NULL;
  }

  if (NULL == p_pKey || 0 == p_uLen)
  {
    dns_log("Parameters for binary key are not sane: data is NULL? %d, length = %u\n",
            NULL == p_pKey,
            (unsigned) p_uLen);
  }
  else
  {
    m_pBinKey = new u_char[p_uLen];
    m_uBinKeyLen = p_uLen;
    memset(m_pBinKey, 0, p_uLen);

    memcpy(m_pBinKey, p_pKey, m_uBinKeyLen);
    m_sKey = base64_encode((const unsigned char *) m_pBinKey, m_uBinKeyLen);
    dns_log("Just set key '%s' with length %u\n", m_sKey.c_str(), (unsigned) m_uBinKeyLen);
  }
}

int DnsDnskey::calcKeyTag()
{
  int iRet = -1;

  std::vector< u_char > oBytes;
  u_char *pKey = get_rdata();
  size_t uKeyLen = get_rdlen();
  if (NULL == pKey)
  {
    dns_log("No rdata set, reconstituting packet.\n");
    dns_dnskey_header_t tKey;
    memset(&tKey, 0, sizeof(tKey));
    tKey.m_uFlags = htons(getFlags());
    tKey.m_uProto = getProto();
    tKey.m_uAlgo = getAlgo();
    oBytes.insert(oBytes.end(), (u_char *) &tKey, (u_char *) &tKey + sizeof(tKey));
    oBytes.insert(oBytes.end(), (u_char *) getBinKey(), (u_char *) getBinKey() + getBinKeyLen());
    pKey = oBytes.data();
    uKeyLen = oBytes.size();
  }

  if (NULL == pKey)
  {
    dns_log("No key set.\n");
  }
  else if (0 == uKeyLen)
  {
    dns_log("Key length cannot be 0\n");
  }
  else
  {
    uint32_t uTmpRet = 0;

    for (size_t u = 0; u < uKeyLen; u++)
    {
      uTmpRet += (u & 1) ? pKey[u] : pKey[u] << 8;
    }
    uTmpRet += (uTmpRet >> 16) & 0xffff;
    uTmpRet &= 0xffff;

    iRet = (int) uTmpRet;
  }

  return iRet;
}

bool DnsDnskey::parseRData(u_char *p_pMsg,
                           size_t p_uMsgLen,
                           u_char *p_pRData,
                           size_t p_uRDataLen)
{
  bool bRet = false;

  if (!isQuestion() && p_uRDataLen < 5)
  {
    dns_log("DNSKEY is not long enough: %u\n", (unsigned) p_uRDataLen);
  }
  else if (!isQuestion())
  {
    set_rdata(p_pRData, p_uRDataLen);

    setFlags(ntohs(*(uint16_t *)p_pRData));
    p_pRData += sizeof(uint16_t);
    p_uRDataLen -= sizeof(uint16_t);

    uint8_t uProto = (ntohs(*(uint16_t *) p_pRData) >> 8) & 0x00ff;
    setProto(uProto);

    uint8_t uAlgo = ntohs(*(uint16_t *) p_pRData) & 0x00ff;
    setAlgo(uAlgo);

    p_pRData += sizeof(uint16_t);
    p_uRDataLen -= sizeof(uint16_t);

    setBinKey(p_pRData, p_uRDataLen);

    // Now setting this from within
    // setBinKey()
    /*
    std::string sKey = base64_encode((const unsigned char *) p_pRData, p_uRDataLen);
    setKey(sKey);
    */

    bRet = true;
  }

  return bRet;
}

DnsDnskey *DnsDnskey::dup()
{
  return new DnsDnskey();
}

void DnsDnskey::printRData()
{
  fprintf(stdout, "%s", toString().c_str());
}

std::string DnsDnskey::toString()
{
  stringstream oSS;
  oSS << (unsigned) getFlags()
      << " " << (unsigned) getProto()
      << " " << (unsigned) getAlgo()
      << " " <<  getKey().c_str();

  return oSS.str();
}

long DnsDnskey::getDnskeyId() {
    return m_dnskeyId;
}

void DnsDnskey::setDnskeyId(long dnskeyId) {
    m_dnskeyId = dnskeyId;
}

