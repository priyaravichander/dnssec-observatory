//
// Created by Priya Ravichander on 6/10/21.
//

#include <iostream>
#include "../../include/MysqlClient.h"
#include "mysql/mysql.h"
#include "../../include/dns/dns_dnskey.h"
#include "../../include/dns/dns_rrsig.h"
#include "../../include/DnssecRRset.h"
#include "../../include/global_variables.h"

using namespace std;

MYSQL *MysqlClient::getConnection(string username, string ip, int port) {
    MYSQL_RES *result;
    MYSQL_ROW row;
    MYSQL *connection, mysql;

    int state;

    mysql_init(&mysql);
    connection = mysql_real_connect(&mysql, ip.c_str(), username.c_str(), "dnssec", "dnssec", port, 0, 0);

    // cout << connection;
    if (connection == NULL) {
        std::cout << mysql_error(&mysql) << std::endl;
        return NULL;
    }
    return connection;
}

int MysqlClient::insertDomainData(map<string, string> domainSoaMap, map<string, long> dnskeyMeasurementIdMap,
                                   long timestamp,
                                   map<string, long> rrsigMeasurementIdMap) {
    MYSQL *connection, mysql;

    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return 0;
    }
    for (map<string, string>::iterator iter = domainSoaMap.begin(); iter != domainSoaMap.end(); iter++) {

        string insertStatement =
                "insert into domains(domain_id, domain, soa, timestamp, dnskey_measurement_id, rrsig_measurement_id) values('" +
                to_string(++nextDomainId) +
                "','" + iter->first + "','" + iter->second + "','" +
                to_string(timestamp) + "','" + to_string(dnskeyMeasurementIdMap.find(iter->second)->second) + "','" +
                to_string(rrsigMeasurementIdMap.find(iter->second)->second) + "')";
//    cout << insertStatement << "\n";
        state = mysql_query(connection, insertStatement.c_str());
        if (state != 0) {
//        cout << "here";
            std::cout << mysql_error(connection) << std::endl;

        }
    }
    mysql_close(connection);

    return nextDomainId;
}


bool
MysqlClient::insertDnsKeyRecords(string dnskeyRelationships, string dnskeyRecords) {

    MYSQL *connection, mysql;

    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return false;
    }

    string dnskeyInsertStatement =
            "insert ignore into dnskey(timestamp, flag, algorithm,dnskey, calckeytag) values " +
            dnskeyRecords.substr(0, dnskeyRecords.size() - 1)  ;

    cout << "\n" << dnskeyInsertStatement << "\n";
    state = mysql_query(connection, dnskeyInsertStatement.c_str());
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;

    }
    string dnskeyRelationshipInsertStatement =
            "insert into dnskey_relationship(created_time,last_updated_time,probe_id, sub_id, calcKeytag ,domain_id) values " +
            dnskeyRelationships.substr(0, dnskeyRelationships.size() - 1) + "ON DUPLICATE KEY UPDATE last_updated_time='" + to_string(std::time(nullptr)) + "'";
    cout << "\n" << dnskeyRelationshipInsertStatement << "\n";
    state = mysql_query(connection, dnskeyRelationshipInsertStatement.c_str());
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;

    }

//    cout << insertStatement << "\n";

    mysql_close(connection);


    return true;


}

bool MysqlClient::insertRRSIGRecords(string rrsigRecords, string rrsigRelationships) {
    MYSQL *connection, mysql;

    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return false;
    }

    string rrsigInsertStatement =
            "insert ignore into rrsig(timestamp, ttl, inception,expiration, keytag, signerName, signature, algorithm, labels) values " +
            rrsigRecords.substr(0, rrsigRecords.size() - 1);

    cout << "\n" << rrsigInsertStatement << "\n";
    state = mysql_query(connection, rrsigInsertStatement.c_str());
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;

    }
    string rrsigRelationshipInsertStatement =
            "insert into rrsig_relationship(created_time, last_updated_time, probe_id, sub_id, keytag,domain_id) values " +
            rrsigRelationships.substr(0, rrsigRelationships.size() - 1)+ "ON DUPLICATE KEY UPDATE last_updated_time='" + to_string(std::time(nullptr)) + "'";
    cout << "\n" << rrsigRelationshipInsertStatement << "\n";
    state = mysql_query(connection, rrsigRelationshipInsertStatement.c_str());
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;

    }


    mysql_close(connection);


    return true;

}

bool MysqlClient::insertDNSSECMeasurementRecords(long probe_id, long dnskeyId, long subid, long domainId) {
    MYSQL *connection, mysql;
//    cout << "subid" << subid;
    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return false;
    }


    string updateStatement = "UPDATE dnskey_relationship SET isVerified='1' where domain_id='"+ to_string(domainId) + "'and probe_id='" + to_string(probe_id) + "'and sub_id='"+to_string(subid)+"'and calckeytag='" + to_string(dnskeyId) +"'";
    cout << "\n" << updateStatement;
    state = mysql_query(connection, updateStatement.c_str());
    mysql_close(connection);
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;

    }

    return true;

}

void MysqlClient::loadIds() {
    MYSQL *connection, mysql;
    MYSQL_RES *result;
    MYSQL_ROW row;
    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {
//        cout << "hersdasde";

        std::cout << mysql_error(&mysql) << std::endl;

    }

    string getMaxDnskeyId = "Select (select count(*) from dnskey) as Count1, (select count(*) from rrsig) as Count2, (select count(*) from domains) as Count3";
    state = mysql_query(connection, getMaxDnskeyId.c_str());
//    cout << getMaxDnskeyId << "\n";
    result = mysql_store_result(connection);
    mysql_close(connection);
    if (state != 0) {

        std::cout << mysql_error(connection) << std::endl;

    }
//
    ;
    while ((row = mysql_fetch_row(result)) != NULL && row[0] != NULL) {
        nextDnskeyId = stol(row[0]);
        nextRRsigId = stol(row[1]);
        nextDomainId = stol(row[2]);
//        cout << row[0] << " " << row[1] << " " << row[2] << " ";
//        cout <<"\nnext domain id" << nextDomainId << "\n";

    }


}

list<tuple<string, string>> MysqlClient::fetchDomainRecords() {
    MYSQL *connection, mysql;
    MYSQL_RES *result;
    MYSQL_ROW row;
    list<tuple<string, string> > measurementIdList;
    int state;

    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return measurementIdList;
    }

    string soaQueryStatement = "select dnskey_measurement_id, rrsig_measurement_id from domains";
    state = mysql_query(connection, soaQueryStatement.c_str());
    result = mysql_store_result(connection);


    mysql_close(connection);
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;
        return measurementIdList;
    }


    while ((row = mysql_fetch_row(result)) != NULL) {
//        cout << row[0] << std::endl;
        measurementIdList.push_back(tuple<string, string>(row[0], row[1]));


    }


    return measurementIdList;


}

tuple<long, string, long, long> MysqlClient::getMeasurementIdsForDomain(string domain) {
    MYSQL *connection, mysql;
    MYSQL_RES *result;
    MYSQL_ROW row;
    int state;
    tuple<long,string, long, long> measurementId;
    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return measurementId;
    }

    string soaQueryStatement =
            "select domain_id, soa,dnskey_measurement_Id,rrsig_measurement_Id from domains where domain='" + domain + "'";
    state = mysql_query(connection, soaQueryStatement.c_str());
    result = mysql_store_result(connection);


    mysql_close(connection);
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;
        return measurementId;
    }


    while ((row = mysql_fetch_row(result)) != NULL) {
//        cout << row[0] << std::endl;
        measurementId = make_tuple(stol(row[0]), row[1], stol(row[2]), stol(row[3]));
    }

    return measurementId;

}

tuple<long, string, long, long> MysqlClient::getMeasurementIdsForSOA(string soa) {
    MYSQL *connection, mysql;
    MYSQL_RES *result;
    MYSQL_ROW row;
    int state;
    tuple<long,string, long, long> measurementId;
    mysql_init(&mysql);

    connection = mysql_real_connect(&mysql, "127.0.0.1", "root", "dnssec", "dnssec", 3306, 0, 0);

    if (connection == NULL) {


        std::cout << mysql_error(&mysql) << std::endl;
        return measurementId;
    }

    string soaQueryStatement =
            "select domain_id, soa,dnskey_measurement_Id,rrsig_measurement_Id from domains where soa='" + soa + "'";
    state = mysql_query(connection, soaQueryStatement.c_str());
    result = mysql_store_result(connection);


    mysql_close(connection);
    if (state != 0) {
//        cout << "here";
        std::cout << mysql_error(connection) << std::endl;
        return measurementId;
    }


    while ((row = mysql_fetch_row(result)) != NULL) {
//        cout << row[0] << std::endl;
        measurementId = make_tuple(stol(row[0]), row[1], stol(row[2]), stol(row[3]));
    }

    return measurementId;
}
