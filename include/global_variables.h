//
// Created by Priya Ravichander on 6/13/21.
//

#ifndef DNSSEC_OBSERVATORY_GLOBAL_VARIABLES_H
#define DNSSEC_OBSERVATORY_GLOBAL_VARIABLES_H


#include <string>
#include <map>
using namespace std;

extern long nextDnskeyId;
extern long nextRRsigId;
extern long nextDomainId;
extern std::map<long, string> probeMap;



#endif //DNSSEC_OBSERVATORY_GLOBAL_VARIABLES_H
