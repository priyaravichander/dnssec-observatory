//
// Created by Priya Ravichander on 6/4/21.
//

#ifndef DNSSEC_OBSERVATORY_MEASUREMENT_HANDLER_H
#define DNSSEC_OBSERVATORY_MEASUREMENT_HANDLER_H
#include <string>
#include <vector>
#include <list>
#include <map>
using namespace std;

class RipeAtlasHandler {
public:
    map<string, string> getRipeAtlasMeasurement(map<string ,long> measurementIds);
    map<string,long> createRipeAtlasMeasurement(map<string, string> domainMap, string queryType, bool doFlag);
};


#endif //DNSSEC_OBSERVATORY_MEASUREMENT_HANDLER_H
