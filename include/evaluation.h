//
// Created by Priya Ravichander on 6/8/21.
//

#ifndef DNSSEC_OBSERVATORY_EVALUATION_H
#define DNSSEC_OBSERVATORY_EVALUATION_H


#include "Validator.h"
#include "../include/proberesult.h"
#include "../include/DnssecRRset.h"
#include "DnssecMeasurement.h"
#include <map>

class Evaluation {
public:

    map<string, map<std::string, list<struct DnsDnskey *>>>
    groupDnskeyRecords(map<string, ProbeResult> probeResult, long domainId);
    map<string, map<std::string, list<struct DnsRR *>>>
    groupRRsigRecords(map<string, ProbeResult> rrsigProbeResult, long domainId);

    map<uint16_t, list<DnssecRRset *>>
    groupMatchingKeys(map<string, list<DnsDnskey *>> dnskeyMap, map<string, list<DnsRR *>> rrsigMap,
                      map<uint16_t, list<string>> *keyAndProbeIdListMap,
                      map<string, list<uint16_t>> *probeIdKeytagListMap);

    map<string, DnssecRRset *>
    groupRecordsByProbe(map<string, list<DnsDnskey *>> dnskeyGroup, map<string, list<DnsRR *>> rrsigGroup);

    list<DnssecMeasurement>
    verifyKeys(list<DnsDnskey *> dnskeyList, list<DnsRrsig *> rrsigList, string probe_sub_id, long domainId);
};


#endif //DNSSEC_OBSERVATORY_EVALUATION_H
