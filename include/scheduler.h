//
// Created by Priya Ravichander on 6/21/21.
//

#ifndef DNSSEC_OBSERVATORY_SCHEDULER_H
#define DNSSEC_OBSERVATORY_SCHEDULER_H


class Scheduler {
public:
    void initiateTimer();
    void scheduleTask();

    void scheduleTaskForPollingDomains();
    void pollAndMeasureRecords(long dnskeyId, long rrsigId);
};


#endif //DNSSEC_OBSERVATORY_SCHEDULER_H
