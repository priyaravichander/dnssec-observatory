/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef __DNS_TLSA_H__
#define __DNS_TLSA_H__

#include <stdint.h>
#include <inttypes.h>

#include <string>

#include "dns_rr.h"
#include "dns_defs.h"

class DnsTlsa : public DnsRR
{
  private:
    uint8_t m_uUsage;
    uint8_t m_uSelector;
    uint8_t m_uMatching;
    int m_iCADataLen;
    u_char *m_pCAData;
    std::string m_sCAD;

  public:
    DnsTlsa();
    DnsTlsa(const DnsName &, std::string &p_sName);
    virtual ~DnsTlsa();

    // returns true if there are four bytes of RDATA
    virtual bool rdata_valid();

    uint8_t getUsage();
    void setUsage(uint8_t p_uUsage);

    uint8_t getSelector();
    void setSelector(uint8_t p_uSelector);

    uint8_t getMatching();
    void setMatching(uint8_t p_uMatching);

    u_char *getCertAssociationData();
    int getCertAssociationDataLen();
    void setCertAssociationData(u_char *p_pBuff, int p_iLen);

    bool setCertAssociationDataStr(std::string &p_sCAD);
    std::string &getCertAssociationDataStr();

    virtual bool operator==(const DnsRR &p_oRHS);
    virtual bool operator==(const DnsTlsa &p_oRHS);
    virtual bool operator==(DnsTlsa &p_oRHS);
    virtual DnsTlsa &operator=(const DnsRR &p_oRHS);
    virtual DnsTlsa &operator=(const DnsTlsa &p_oRHS);

    virtual DnsTlsa *dup();
    virtual void printRData();
    virtual std::string toString();
    virtual bool fromString(std::string &p_sRdata);
  protected:
    virtual bool parseRData(u_char *p_pMsg,
                            size_t p_uMsgLen,
                            u_char *p_pRData,
                            size_t p_uRDataLen);

};

#endif /* __DNS_TLSA_H__ */
