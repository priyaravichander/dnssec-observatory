/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef __DNS_PACKET_H__
#define __DNS_PACKET_H__

#include "dns_defs.h"
#include "dns_header.h"
#include "dns_compression.h"

class DnsRR;
class DnsHeader;
class DnsTsig;
class DnsRRset;
class DnsOpt;

class DnsPacket
{
  private:
    bool m_bParsed;
    u_char *m_pBytes;
    size_t m_uBytesLen;
    size_t m_uRecvLen;

    DnsTsig *m_pTsig;
    DnsOpt *m_pOpt;

    DnsHeader m_oHeader;
    DnsCompression m_oComp;

    RRList_t m_qd;
    RRList_t m_an;
    RRList_t m_ns;
    RRList_t m_ar;

  public:
    DnsPacket();
    DnsPacket(bool question, int id = -1);
    virtual ~DnsPacket();

    int toWire(u_char *p_pBuff, size_t p_uBuffLen, bool p_bAddTsig = true);
    bool fromWire(u_char *p_pBuff, size_t p_uBuffLen);

    DnsHeader &getHeader();
    bool getQuestions(RRList_t &p_oQuestionsList);
    bool getAnswers(RRList_t &p_oAnswersList);
    bool getAuthority(RRList_t &p_oAuthorityList);
    bool getAdditional(RRList_t &p_oAdditionalList);

    bool getAnswers(DnsRRset &p_oSet);
    bool getAuthority(DnsRRset &p_oSet);
    bool getAdditional(DnsRRset &p_oSet);

    void setTsig(DnsTsig *p_pTsig);
    bool hasEmbeddedTsig();
    DnsTsig *getTsig();

    DnsOpt *getOpt();
    void setOpt(DnsOpt *p_pOpt);

    void addQuestion(DnsRR &p_oRR);
    void addAnswer(DnsRR &p_oRR);
    void addAuthority(DnsRR &p_oRR);
    void addAdditional(DnsRR &p_oRR);

    void print();
    void clear();

    size_t getRecvLen();

  private:

    int toWire(RRList_t &p_oList, size_t p_uOffset);
    DnsCompression &getCompression();
    u_char *getMsg();
    size_t getMsgLen();
    void setMsg(u_char *p_pMsg, size_t p_uLen);
};

#endif
