/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef __DNS_OPT_H__
#define __DNS_OPT_H__

#include <stdint.h>

#include <string>

#include "dns_rr.h"

class DnsName;

typedef struct
{
  uint16_t  m_uCode;
  uint16_t  m_uLen;
  char     *m_pValue;
} opt_code_t;

typedef std::list<opt_code_t> OptList_t;
//typedef OptList_t::const_iterator OptIter_t;
typedef OptList_t::iterator OptIter_t;

class DnsOpt : public DnsRR
{
  // Member Variables
  private:
    OptList_t m_oOpts;

  // Methods
  public:
    DnsOpt(uint16_t p_uMax = 512, bool p_bDO = false);
    DnsOpt(DnsOpt &p_oRHS);
    virtual ~DnsOpt();

    virtual bool operator==(const DnsRR &p_oRHS);
    virtual bool operator==(const DnsOpt &p_oRHS);
    virtual bool operator==(DnsOpt &p_oRHS);
    virtual DnsOpt &operator=(const DnsRR &p_oRHS);
    virtual DnsOpt &operator=(const DnsOpt &p_oRHS);

    // returns true if there are four bytes of RDATA
    virtual bool rdata_valid();

    uint16_t getMax();
    void setMax(uint16_t p_uMax);

    int setFlag(uint8_t p_uPos, bool p_bVal);
    void setVersion(uint8_t p_uVer);
    void setRcode(uint8_t p_uVer);

    bool getDO();
    void setDO(bool p_bDO);

    virtual bool isQuestion();

    virtual DnsOpt *dup();

    virtual int rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp);

    virtual void printRData();
    virtual std::string toString();

    bool addOption(uint16_t p_uCode,
                   uint16_t p_uLength,
                   char *p_pValue);
    bool deleteOption(uint16_t p_uCode);
    OptIter_t begin();
    OptIter_t end();

    bool clearOpts();
protected:
    virtual bool parseRData(u_char *p_pMsg,
                            size_t p_uMsgLen,
                            u_char *p_pRData,
                            size_t p_uRDataLen);
};

#endif /* __DNS_OPT_H__ */
