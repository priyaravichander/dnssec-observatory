/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef __DNS_NS_H__
#define __DNS_NS_H__

#include <stdint.h>

#include <string>

#include "dns_rr.h"

class DnsName;
class DnsCompression;
class DnsNs : public DnsRR
{
  private:
    std::string m_sName;

  public:
    DnsNs();
    DnsNs(const DnsName &, std::string &p_sName);
/*
    DnsNs(const DnsName &, int p_iLen, char *p_pRData);
*/
    virtual ~DnsNs();

    // returns true if there are four bytes of RDATA
    virtual bool rdata_valid();

    std::string &getName();
    void setName(std::string &p_sName);

    virtual bool operator==(const DnsRR &p_oRHS);
    virtual bool operator==(const DnsNs &p_oRHS);
    virtual bool operator==(DnsNs &p_oRHS);
    virtual DnsNs &operator=(const DnsRR &p_oRHS);
    virtual DnsNs &operator=(const DnsNs &p_oRHS);

    virtual DnsNs *dup();
    virtual void printRData();
    virtual std::string toString();
    virtual bool fromString(std::string &p_sRdata);
    virtual int rdata_to_wire(u_char *p_pBuff, size_t p_uLen, DnsCompression &p_oComp);//Xiaoyan HU add
  protected:
    virtual bool parseRData(u_char *p_pMsg,
                            size_t p_uMsgLen,
                            u_char *p_pRData,
                            size_t p_uRDataLen);

    virtual int verificationRData(DnsBits_t &p_oVec);
};

#endif /* __DNS_NS_H__ */
