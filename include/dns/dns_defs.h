/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _DNS_DEFS_H
#define _DNS_DEFS_H

#include "config.h"

#ifdef HAVE_INTTYPES_H
#include <inttypes.h>
#endif

#include <list>
#include <vector>
#include <string>

#ifdef _DNS_DEBUG
#define dns_log(X, ...) fprintf(stderr, "%s [%d] - " X, __FILE__, __LINE__, ##__VA_ARGS__)
#else
#define dns_log(X, ...)
#endif

#define DNS_RR_A 1
#define DNS_RR_NS 2
#define DNS_RR_MD 3
#define DNS_RR_MF 4
#define DNS_RR_CNAME 5
#define DNS_RR_SOA 6
#define DNS_RR_MB 7
#define DNS_RR_MG 8
#define DNS_RR_MR 9
#define DNS_RR_NULL 10
#define DNS_RR_WKS 11
#define DNS_RR_PTR 12
#define DNS_RR_HINFO 13
#define DNS_RR_MINFO 14
#define DNS_RR_MX 15
#define DNS_RR_TXT 16
#define DNS_RR_RP 17
#define DNS_RR_AFSDB 18
#define DNS_RR_X25 19
#define DNS_RR_ISDN 20
#define DNS_RR_RT 21
#define DNS_RR_NSAP 22
#define DNS_RR_NSAP_PTR 23
#define DNS_RR_SIG 24
#define DNS_RR_KEY 25
#define DNS_RR_PX 26
#define DNS_RR_GPOS 27
#define DNS_RR_AAAA 28
#define DNS_RR_LOC 29
#define DNS_RR_NXT 30
#define DNS_RR_EID 31
#define DNS_RR_NIMLOC 32
#define DNS_RR_SRV 33
#define DNS_RR_ATMA 34
#define DNS_RR_NAPTR 35
#define DNS_RR_KX 36
#define DNS_RR_CERT 37
#define DNS_RR_A6 38
#define DNS_RR_DNAME 39
#define DNS_RR_SINK 40
#define DNS_RR_OPT 41
#define DNS_RR_APL 42
#define DNS_RR_DS 43
#define DNS_RR_SSHFP 44
#define DNS_RR_IPSECKEY 45
#define DNS_RR_RRSIG 46
#define DNS_RR_NSEC 47
#define DNS_RR_DNSKEY 48
#define DNS_RR_DHCID 49
#define DNS_RR_NSEC3 50
#define DNS_RR_NSEC3PARAM 51
#define DNS_RR_TLSA 52
#define DNS_RR_HIP 55
#define DNS_RR_NINFO 56
#define DNS_RR_RKEY 57
#define DNS_RR_TALINK 58
#define DNS_RR_SPF 99
#define DNS_RR_UINFO 100
#define DNS_RR_UID 101
#define DNS_RR_GID 102
#define DNS_RR_UNSPEC 103
#define DNS_RR_TKEY 249
#define DNS_RR_TSIG 250
#define DNS_RR_IXFR 251
#define DNS_RR_AXFR 252
#define DNS_RR_MAILB 253
#define DNS_RR_MAILA 254

#define DNS_CLASS_IN 1
#define DNS_CLASS_CH 3
#define DNS_CLASS_ANY 255

#define DNS_DS_SHA1 1
#define DNS_DS_SHA256 2

#define DNS_DEFAULT_PORT 53

// Room for a max message size + a huge TSIG RR
#define DNS_UDP_MAX_PACKET_SIZE 4096
#define DNS_TCP_MAX_PACKET_SIZE 65536
// Room for a max message size + a huge TSIG RR
#define DNS_UDP_TSIG_MAX_BUFSIZ DNS_UDP_MAX_PACKET_SIZE + 255
#define DNS_QUERY_MAX_PACKET_SIZE DNS_UDP_TSIG_MAX_BUFSIZ

#define DNS_TSIG_DEFAULT_FUDGE 300


// The overall name can only be 253 characters, so
// there is no explici number of lables, but if
// each is 1 character, thsi is the implicit max
#define DNS_MAX_LABELS 253

enum rcode_t
{
    DNS_NOERROR = 0, DNS_FORMERR, DNS_SERVFAIL, DNS_NXDOMAIN, DNS_NOTIMP,
    DNS_REFUSED, DNS_YXDOMAIN, DNS_YXRRSET, DNS_NXRRSET, DNS_NOTAUTH,
    DNS_NOTZONE, DNS_BADSIG, DNS_BADKEY, DNS_BADTIME
};

typedef enum
{
  DNS_RES_FALLBACK = 0,
  DNS_RES_FORCE,
  DNS_RES_DISABLE
} dns_res_tcp_e;

typedef enum
{
  TASK_ERROR = -1,
  PRE_INIT = 0,
  TASK_SENDING,
  TASK_RECEIVING,
  TASK_DONE
} DnsTaskStatus_e;

class DnsRR;

struct q_header
{
  int16_t type;
  int16_t qclass;
} __attribute__((__packed__));

struct rr_header
{
  q_header q;
  int32_t ttl;
  uint16_t rdlen;
} __attribute__((__packed__));

typedef struct 
{
  uint16_t m_uFlags;
  uint8_t m_uProto;
  uint8_t m_uAlgo;
} __attribute__((__packed__)) dns_dnskey_header_t;

typedef unsigned char u_char;

typedef std::list<DnsRR *> RRList_t;
typedef RRList_t::iterator RRIter_t; 
typedef RRList_t::const_iterator KRRIter_t; 

typedef std::vector<u_char> DnsBits_t;
typedef DnsBits_t::iterator DnsBitIter_t;

typedef std::list<std::string> TxtList_t;
typedef TxtList_t::iterator    TxtIter_t;

typedef std::list<uint16_t>           RRtypeList_t;
typedef RRtypeList_t::iterator   RRtypeIter_t;

#endif
