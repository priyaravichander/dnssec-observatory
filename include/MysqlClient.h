//
// Created by Priya Ravichander on 6/10/21.
//

#ifndef DNSSEC_OBSERVATORY_MYSQLCLIENT_H
#define DNSSEC_OBSERVATORY_MYSQLCLIENT_H



#include <string>


#include "MysqlClient.h"
#include "dns/dns_dnskey.h"
#include "dns/dns_rrsig.h"
#include "DnssecRRset.h"
#include <../../../../../../../../usr/local/Cellar/mysql-client/8.0.25/include/mysql/mysql.h>


#include <map>

using namespace std;

class MysqlClient {
public:
    MYSQL* getConnection(string username, string ip, int port);
    int insertDomainData(map<string, string> domainSoaMap, map<string, long> dnskeyMeasurementIdMap, long timestamp,
                          map<string, long> rrsigMeasurementIdMap);

    tuple<long, string, long, long> getMeasurementIdsForDomain(string domain);


    bool insertDnsKeyRecords(string dnskeyRelationships, string dnskeyRecords);

    bool insertRRSIGRecords(string rrsigRecords, string rrsigRelationships);

    bool
    insertDNSSECMeasurementRecords(long probe_id, long dnskeyId, long subid, long domainId);

    void loadIds();

    list<tuple<string, string>> fetchDomainRecords();

//    tuple<string, long, long> getMeasurementIdsForDomain(string domain);
    tuple<long, string, long, long> getMeasurementIdsForSOA(string soa);
};


#endif //DNSSEC_OBSERVATORY_MYSQLCLIENT_H