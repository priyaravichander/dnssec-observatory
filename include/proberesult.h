//
// Created by Priya Ravichander on 6/8/21.
//

#ifndef DNSSEC_OBSERVATORY_PROBERESULT_H
#define DNSSEC_OBSERVATORY_PROBERESULT_H

#include "probeinfo.h"
#include "measurement.h"

using namespace std;

class ProbeResult {
public:
    list<ProbeInfo*> probeInfo;

};


#endif //DNSSEC_OBSERVATORY_PROBERESULT_H
