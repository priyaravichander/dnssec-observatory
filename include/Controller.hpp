#ifndef MyController_hpp
#define MyController_hpp

#include "dto/DTOs.hpp"

#include "oatpp/web/server/api/ApiController.hpp"
#include "oatpp/core/macro/codegen.hpp"
#include "oatpp/core/macro/component.hpp"

#include OATPP_CODEGEN_BEGIN(ApiController) //<-- Begin Codegen
#include "poller.h"

/**
 * Sample Api Controller.
 */
class Controller : public oatpp::web::server::api::ApiController {
public:
  /**
   * Constructor with object mapper.
   * @param objectMapper - default object mapper used to serialize/deserialize DTOs.
   */
  Controller(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>, objectMapper))
    : oatpp::web::server::api::ApiController(objectMapper)
  {}
public:


  ENDPOINT("GET", "/observatory/{path-param}", root,REQUEST(std::shared_ptr<IncomingRequest>, request), // Map request object to endpoint method

           PATH(String, domain, "path-param")) {


        auto dto = MyDto::createShared();
    dto->statusCode = 200;
    dto->message = "Hello World this this that!";

//        (new poller())->main1(domain->c_str());
        Poller poller;

        dto->message = poller.performPoll(domain->c_str()).c_str();

    return createDtoResponse(Status::CODE_200, dto);
  }
  

};

#include OATPP_CODEGEN_END(ApiController) //<-- End Codegen

#endif /* MyController_hpp */
