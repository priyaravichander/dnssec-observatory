//
// Created by Priya Ravichander on 6/22/21.
//

#ifndef DNSSEC_OBSERVATORY_DNSSECMEASUREMENT_H
#define DNSSEC_OBSERVATORY_DNSSECMEASUREMENT_H



#include <string>
using namespace std;
class DnssecMeasurement {
public:
    bool isKey = false;
    long probeId = 0;
    long subId = 0;
    long sigkeyTag = -1;
    long dnskeyKeytag = -1;
    string signature = "";
    string key = "";
    bool isVerified = false;
};


#endif //DNSSEC_OBSERVATORY_DNSSECMEASUREMENT_H
