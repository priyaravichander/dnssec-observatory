//
// Created by Priya Ravichander on 6/7/21.
//

#ifndef DNSSEC_OBSERVATORY_REST_CLIENT_H
#define DNSSEC_OBSERVATORY_REST_CLIENT_H
#include <curl/curl.h>
#include <string>
using namespace std;

class RESTClient {
public:
    string fireGetRequest(string url);
    string firePostRequest(string url, string data);
};


#endif //DNSSEC_OBSERVATORY_REST_CLIENT_H
