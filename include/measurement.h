//
// Created by Priya Ravichander on 6/8/21.
//

#ifndef DNSSEC_OBSERVATORY_MEASUREMENT_H
#define DNSSEC_OBSERVATORY_MEASUREMENT_H
#include "string.h"
#include "dns/dns_packet.h"

using namespace std;
class Measurement {
public:
    long timestamp;
    int lts;
    int subid;
    int submax;
    string destinationAddress;
    string destinationPort;
    int af;
    string sourceAddress;
    string protocol;
    float rt;
    string abuf;
    DnsPacket* result;
    Measurement();
    virtual ~Measurement();

    string soaName;


};


#endif //DNSSEC_OBSERVATORY_MEASUREMENT_H
