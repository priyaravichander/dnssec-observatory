//
// Created by Priya Ravichander on 6/8/21.
//

#ifndef DNSSEC_OBSERVATORY_PROBEINFO_H
#define DNSSEC_OBSERVATORY_PROBEINFO_H
#include <string>
#include "measurement.h"

using namespace std;

class ProbeInfo {
public:
    long probeid;
    long measurementId;
    long timestamp;
    string msm_name;
    string from;
    string type;
    long group_id;
    long storedTimestamp;
    list<Measurement*> measurement;
    ProbeInfo();
    virtual ~ProbeInfo();

};


#endif //DNSSEC_OBSERVATORY_PROBEINFO_H
