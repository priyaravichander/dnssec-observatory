//
// Created by Priya Ravichander on 6/4/21.
//

#ifndef DNSSEC_OBSERVATORY_VALIDATOR_H
#define DNSSEC_OBSERVATORY_VALIDATOR_H

#pragma once
#include <string>
#include <vector>
#include <dns.h>
#include <nlohmann/json.hpp>
#include "dns/dns_rr.h"

#include "dns/dns_header.h"
#include "dns/dns_tsig.h"
#include "dns/dns_opt.h"
#include "proberesult.h"
using namespace std;

class Validator {
private:
    bool m_bParsed;
    u_char *m_pBytes;
    size_t m_uBytesLen;
    size_t m_uRecvLen;

    DnsTsig *m_pTsig;
    DnsOpt *m_pOpt;

    DnsHeader m_oHeader;
    DnsCompression m_oComp;

    RRList_t m_qd;
    RRList_t m_an;
    RRList_t m_ns;
    RRList_t m_ar;

public:

     bool isValidInput(string domain);
    std::vector<unsigned char> base64_decode(string const& encoded_string);
     DnsPacket* parseDecoding(string decodedString);
    bool fromWire(u_char *p_pBuff, size_t p_uBuffLen);
    map<string, string> extractSOAData(map<string, string> resultJson);
    map<string, ProbeResult> extractRipeAtlasData(map<string, string> resultJson);

 };

#endif //DNSSEC_OBSERVATORY_VALIDATOR_H
