//
// Created by Priya Ravichander on 7/1/21.
//

#ifndef DNSSEC_OBSERVATORY_POLLER_H
#define DNSSEC_OBSERVATORY_POLLER_H

class Poller {
public:

    string performPoll(string domain);
};


#endif //DNSSEC_OBSERVATORY_POLLER_H
