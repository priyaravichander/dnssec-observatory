//
// Created by Priya Ravichander on 6/13/21.
//

#ifndef DNSSEC_OBSERVATORY_DNSSECRRSET_H
#define DNSSEC_OBSERVATORY_DNSSECRRSET_H
#include "dns/dns_dnskey.h"
#include "dns/dns_rrsig.h"

class DnssecRRset {
public:
    DnsDnskey* dnskey;
    DnsRrsig* rrsig;
    long probeId;
    long subId;
    long keytag;
    bool isVerified;
    std::list<DnsDnskey*> dnskeyList;
    std::list<DnsRR*> rrsigList;
    std::string domainId;

    DnssecRRset();
    ~DnssecRRset();

};


#endif //DNSSEC_OBSERVATORY_DNSSECRRSET_H
